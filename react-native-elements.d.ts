import { Theme } from "react-native-elements";

declare module "react-native-elements" {
  export interface Spacing {
    small: number;
    medium: number;
    large: number;
    xLarge: number;
  }
  export interface IFontSize {
    xSmall: number;
    small: number;
    medium: number;
    large: number;
    xLarge: number;
    xxLarge: number;
  }

  export interface FullTheme extends Theme<Spacing> {
    Spacing: Spacing;
    fontSizes: IFontSize;
  }
}
