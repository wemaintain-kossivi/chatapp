import { StatusBar } from "expo-status-bar";
import React from "react";
import { SafeAreaProvider } from "react-native-safe-area-context";
import { I18n } from "react-polyglot";
import { ThemeProvider } from "react-native-elements";
import { Provider as StoreProvider } from "react-redux";

import { locale, phrases } from "./src/i18n";
import useCachedResources from "./src/hooks/useCachedResources";
import Navigation from "./src/navigation";
import theme from "./src/UI/Theme";

import store from "./src/store";

export default function App() {
  const isLoadingComplete = useCachedResources();

  if (!isLoadingComplete) {
    return null;
  } else {
    return (
      <StoreProvider store={store}>
        <I18n locale={locale} messages={phrases}>
          <SafeAreaProvider>
            <ThemeProvider theme={theme}>
              <Navigation />
              <StatusBar />
            </ThemeProvider>
          </SafeAreaProvider>
        </I18n>
      </StoreProvider>
    );
  }
}
