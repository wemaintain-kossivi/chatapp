# TODOS

     : https://gist.github.com/gromon42/8680cffa764045910e400a785ca41976

Login

    - [x] The user should be able to submit a nickname of his choice on the first page to access the chat

Chat

    - [x] One single public channel with every logged-in user displayed on the side
    - [x] The main content area should display 25 messages, and should load more if the users scrolls all the way to the top (Like Slack, Hipchat, etc.)
    - [x] The user should be able to send messages using an input field below the main content area.​

Bonus

    - [x] Expand youtube links to display the Thumbnail and basic details about a video in the chat
    - [x] Implement tabs to switch between channels
    - [x] Implement channel creation

    - [ ] Implement 1-to-1 channels (Private chats)

-- Extras
    
    - [x] I18n ( English and French )
    - [x] Serchbar to filter channel
    - [~] Notifs ( WIP )
    - [~] group channels
    - [~] Profil settings ( WIP )
   
# Intro

project based on react native ( Expo ) and SendBird API

    TypeScript
    React Native
    Expo 
    
    Redux
    Redux Saga 
    React Navigation
    
    Reanimated 2
    React Native Elements

    Jest
    react-test-renderer

 
Plattforms :
    
    IOs (good)
    Android (it may have some UI issues )
   
# Demos

    Steps to run the demo on device ( release mode via Expo Go )
    1) Expo client On the device : https://expo.io/client
    2) Onpen release link to load the app in expo client
    
    link : https://exp.host/@weidmann/ChatApp_prod/index.exp?release-channel=prod-2&sdkVersion=41.0.0

*== Debug Mode 
    
    1) clone the repo
    2) intall expo cli : npm install -g expo-cli
    3) from app folder install modules and run  : 
- yarn  
- yarn start
- Open the app via debug qr code or a link

# == Deploiement

     yarn publish:prod 

Version, app name, infos can be edited  in env/prod.app.json
You can edit the release channel in package.json


# == Architecture

    ...
    src
        Api                 // Sendbird Api
        features            // Components
            Chat            // Left Panel and App Container
            Channel         // list, edit, open
            Message         // list, input, message item
            Login            // Auth
            Notification     // ( WIP )
            Settings         // ( WIP ) logout,edit profil
        i18n                 // internationalization ( En / Fr)
        navigation            // root Stack, bottomStack ,..
        sagas                 // each feature sagas by folder
        store                 // Redux , reducers, selector, actions,..
        UI                     // usefull components , theming,..
        types.tsx             // common typescript interface
    env
    App.tsx                   // App entry
    sendbird.config.ts        // Senbird APP_ID,..
    ...


# == Tests

    yarn test         // run jest test + summary coverage,.. in cli + report in browser

# issues

1) State issue on channelsList screen. The user need to refresh the screen to see the latest channel he've just created.
 