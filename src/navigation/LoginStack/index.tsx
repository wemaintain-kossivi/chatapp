import * as React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import loginScreen from "../../features/Login";
import { LoginStackParamList } from "../types";

const ChannelsStack = createStackNavigator<LoginStackParamList>();

const LoginNavigator = () => {
  return (
    <ChannelsStack.Navigator screenOptions={{ headerShown: false }}>
      <ChannelsStack.Screen name="SignInScreen" component={loginScreen} />
    </ChannelsStack.Navigator>
  );
};

export default LoginNavigator;
