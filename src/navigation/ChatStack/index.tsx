import * as React from "react";
import { StyleSheet } from "react-native";
import { createStackNavigator } from "@react-navigation/stack";
import { BlurView } from "expo-blur";

import ChatContainer from "../../features/Chat/index";
import Channel from "../../features/Channel";
import ChannelEdit from "../../features/Channel/ChannelEdit";
import ChannelEditUsers from "../../features/Channel/ChannelEditUsers";
import { ChatStackParamList, ChannelStackParamList } from "../types";

const ChatStack = createStackNavigator<ChatStackParamList>();
const ChatEditionStack = createStackNavigator<ChannelStackParamList>();

const ChannelEditStackNavigator = () => {
  return (
    <ChatEditionStack.Navigator
      mode="modal"
      screenOptions={{ headerShown: false }}
    >
      <ChatEditionStack.Screen name="Channel" component={Channel} />
      <ChatEditionStack.Screen name="ChannelEdit" component={ChannelEdit} />
      <ChatEditionStack.Screen
        name="ChannelEditUsers"
        component={ChannelEditUsers}
      />
    </ChatEditionStack.Navigator>
  );
};

const ChannelsNavigator = () => {
  return (
    <ChatStack.Navigator screenOptions={{ headerShown: false }}>
      <ChatStack.Screen name="ChannelsList" component={ChatContainer} />

      <ChatStack.Screen
        component={ChannelEditStackNavigator}
        name="ChannelStack"
      />
    </ChatStack.Navigator>
  );
};

export default ChannelsNavigator;
