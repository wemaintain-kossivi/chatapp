import * as React from "react";
import { Ionicons } from "@expo/vector-icons";
import { useTheme } from "react-native-elements";

interface tabIconName {
  default: React.ComponentProps<typeof Ionicons>["name"];
  focused: React.ComponentProps<typeof Ionicons>["name"];
}
interface IProps {
  name: tabIconName;
  color: string;
  focused: boolean;
}
const TabBarIcon: React.FC<IProps> = (props) => {
  const { name, focused, ...otherProps } = props;
  const iconName = focused ? name.focused : name.default;
  return (
    <Ionicons
      size={30}
      style={{ marginBottom: -3 }}
      {...otherProps}
      name={iconName}
    />
  );
};

export default TabBarIcon;
