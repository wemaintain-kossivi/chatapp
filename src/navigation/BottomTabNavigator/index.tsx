import * as React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createStackNavigator } from "@react-navigation/stack";
import { useTranslate } from "react-polyglot";
import { useTheme } from "react-native-elements";
import { useSelector } from "react-redux";

import TabBarIcon from "./TabBarIcon";
import ChatNavigator from "../ChatStack";
import ProfilNavigator from "../ProfilStack";
import NotificationScreen from "../../features/Notification/NotificationsList";
import { BottomTabParamList } from "../types";

import { appHasNotification } from "../../store/selectors";

const BottomTab = createBottomTabNavigator<BottomTabParamList>();

const BottomTabNavigator = () => {
  const t = useTranslate();
  const { theme } = useTheme();
  const hasNotif = useSelector(appHasNotification);

  const tabBarBadgeOption = hasNotif
    ? {
        tabBarBadge: "",
        tabBarBadgeStyle: {
          transform: [{ scale: 0.4 }],
          marginLeft: -5,
          top: 5,
        },
      }
    : {};

  return (
    <BottomTab.Navigator
      initialRouteName="Chat"
      tabBarOptions={{
        activeTintColor: theme.colors?.primary,
        showLabel: false,
      }}
    >
      <BottomTab.Screen
        name="Chat"
        component={ChatNavigator}
        options={{
          tabBarIcon: ({ color, focused }) => (
            <TabBarIcon
              name={{
                default: "chatbubbles-outline",
                focused: "chatbubbles",
              }}
              focused={focused}
              color={color}
            />
          ),
          title: t("Menu Channels"),
        }}
      />
      <BottomTab.Screen
        name="Notifs"
        component={NotificationScreen}
        options={{
          tabBarIcon: ({ color, focused }) => (
            <TabBarIcon
              name={{
                default: "notifications-outline",
                focused: "notifications",
              }}
              focused={focused}
              color={color}
            />
          ),
          title: t("Menu Notifications"),
          ...tabBarBadgeOption,
        }}
      />
      <BottomTab.Screen
        name="Profil"
        component={ProfilNavigator}
        options={{
          tabBarIcon: ({ color, focused }) => (
            <TabBarIcon
              name={{
                default: "settings-outline",
                focused: "settings",
              }}
              focused={focused}
              color={color}
            />
          ),
          title: t("Menu Settings"),
        }}
      />
    </BottomTab.Navigator>
  );
};

export default BottomTabNavigator;
