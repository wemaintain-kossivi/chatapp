import * as React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import SettingsScreen from "../../features/Settings";
import { SettingsStackParamList } from "../types";

const ChannelsStack = createStackNavigator<SettingsStackParamList>();

const SettingsNavigator = () => {
  return (
    <ChannelsStack.Navigator screenOptions={{ headerShown: false }}>
      <ChannelsStack.Screen name="ProfilScreen" component={SettingsScreen} />
    </ChannelsStack.Navigator>
  );
};

export default SettingsNavigator;
