import * as React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { useSelector } from "react-redux";

import screen404 from "../features/NotFoundScreen";
import OfflineScreen from "../features/OfflineScreen";
import BottomTabNavigator from "./BottomTabNavigator";
import LoginStackNavigator from "./LoginStack";
import { RootStackParamList } from "./types";
import { isLogOn, appIsOffline } from "../store/selectors";

const Stack = createStackNavigator<RootStackParamList>();

const RootNavigator = () => {
  const isLogOnVal = useSelector(isLogOn);
  const isOffLine = useSelector(appIsOffline);
  if (isOffLine)
    return (
      <Stack.Navigator screenOptions={{ headerShown: false }}>
        <Stack.Screen name="Offline" component={OfflineScreen} />
      </Stack.Navigator>
    );
  return !isLogOnVal ? (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen name="Login" component={LoginStackNavigator} />
    </Stack.Navigator>
  ) : (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen name="Root" component={BottomTabNavigator} />
      <Stack.Screen
        name="NotFound"
        component={screen404}
        options={{ title: "Oops!" }}
      />
    </Stack.Navigator>
  );
};

export default RootNavigator;
