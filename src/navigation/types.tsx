export type RootStackParamList = {
  Login: undefined;
  Root: undefined;
  NotFound: undefined;
  Offline: undefined;
};
export type LoginStackParamList = {
  SignInScreen: undefined;
};

export type BottomTabParamList = {
  Chat: undefined;
  Notifs: undefined;
  Profil: undefined;
};

export type ChatStackParamList = {
  ChannelsList: undefined;
  ChannelStack: undefined;
};
export type ChannelStackParamList = {
  Channel: undefined;
  ChannelEdit: undefined;
  ChannelEditUsers: undefined;
};
export type SettingsStackParamList = {
  ProfilScreen: undefined;
};
