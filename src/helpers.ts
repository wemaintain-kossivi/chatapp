import {
  isToday,
  differenceInDays,
  differenceInHours,
  differenceInMinutes,
  differenceInSeconds,
} from "date-fns";

export const lastSeenLabel = (ms: string, now = new Date()) => {
  if (ms == "0") return "";
  const secDiff = differenceInSeconds(now, parseInt(ms));
  if (secDiff < 60) return secDiff + "s";
  const minDiff = differenceInMinutes(now, parseInt(ms));
  if (minDiff < 60) return minDiff + " min";
  const hourDiff = differenceInHours(now, parseInt(ms));
  if (hourDiff < 24) return hourDiff + " h";
  return differenceInDays(now, parseInt(ms)) + " j";
};

export const channelTypeLabel = (
  isOpen?: boolean,
  translator?: Function
): string => {
  const rawLabel = isOpen ? "public" : "private";
  if (typeof translator == "function") return translator(rawLabel);
  return rawLabel;
};

export const uniqueMerge = (arr1?: string[], arr2?: string[]) => {
  return [...new Set([...(arr1 || []), arr2 || []])];
};
