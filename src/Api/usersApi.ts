import SendBird, {
  OpenChannel,
  PreviousMessageListQuery,
  User,
  UserMessage,
  UserMessageParams,
} from "sendbird";

import { Map } from "immutable";

import SEND_BIRD_CONFIG from "../../senbird.config";
import {
  IActiveChannel,
  IMap,
  IMapChannel,
  IMapMessage,
  IMapUser,
  ISendPayload,
} from "../types";

import { SB } from "./index";

export const fetchUsers = (): Promise<IMapUser | void> => {
  const appUsersListQuery = SB.createApplicationUserListQuery();

  const processUsers = (users: User[]) => {
    const formated = users.reduce((acc, current: User): {
      [key: string]: User;
    } => {
      return {
        ...acc,
        [current.userId]: current,
      };
    }, {});

    return Map(formated);
  };

  return new Promise((resolve, eject) => {
    appUsersListQuery.next((users, error) => {
      if (error) {
        eject(error);
      } else {
        const u = processUsers(users as User[]);
        resolve(u as IMapUser);
      }
    });
  });
};

export const createMessageListQuery = (
  channel: OpenChannel
): PreviousMessageListQuery => {
  const messageListQuery = channel.createPreviousMessageListQuery();
  return messageListQuery;
};
