import SendBird, {
  OpenChannel,
  PreviousMessageListQuery,
  User,
  UserMessage,
  UserMessageParams,
} from "sendbird";

import { Map } from "immutable";

import SEND_BIRD_CONFIG from "../../senbird.config";
import {
  IActiveChannel,
  IChannelAction,
  IMap,
  IMapChannel,
  IMapMessage,
  ISendPayload,
} from "../types";

import { SB } from "./index";

export const fetchChannel = (
  searchText?: string
): Promise<IMapChannel | void> => {
  const channelsListQuery = SB.OpenChannel.createOpenChannelListQuery();
  if (!!searchText) {
    channelsListQuery.nameKeyword = searchText + "";
  }

  const processChannels = (openChannels: OpenChannel[]) => {
    const formated = openChannels.reduce((acc, current: OpenChannel): {
      [key: string]: OpenChannel;
    } => {
      return {
        ...acc,
        [current.url]: current,
      };
    }, {});

    return Map(formated);
  };

  return new Promise((resolve, eject) => {
    channelsListQuery.next((openChannels, error) => {
      if (error) {
        eject(error);
      } else {
        const chans = processChannels(openChannels);
        resolve(chans as IMapChannel);
      }
    });
  });
};

export const joinChannel = (channel: OpenChannel): Promise<boolean | void> => {
  return new Promise((resolve, eject) => {
    channel.enter((response, error) => {
      if (error) {
        eject(error);
      } else {
        resolve(true);
      }
    });
  });
};

export const editChannel = (
  payload: IChannelAction
): Promise<OpenChannel | undefined> => {
  var params = new SB.OpenChannelParams();
  params.name = payload.chanelName;

  return new Promise((resolve, eject) => {
    SB.OpenChannel.createChannel(
      params,
      (channel: OpenChannel | undefined, error) => {
        if (error) {
          eject(error);
        } else {
          resolve(channel);
        }
      }
    );
  });
};
