import SendBird, {
  OpenChannel,
  PreviousMessageListQuery,
  User,
  UserMessage,
  UserMessageParams,
} from "sendbird";

import { Map } from "immutable";

import SEND_BIRD_CONFIG from "../../senbird.config";
import {
  IActiveChannel,
  IMap,
  IMapChannel,
  IMapMessage,
  ISendPayload,
} from "../types";

import { SB } from "./index";

const FETCH_MSG_LIMIT = 25;

export const sendMessage = (sendPayload: ISendPayload) => {
  const { channel, content } = sendPayload;
  const params = new SB.UserMessageParams();

  params.message = content;

  return new Promise((resolve, eject) => {
    channel.sendUserMessage(params, function (userMessage, error) {
      if (error) {
        eject(error);
      } else {
        const message = userMessage.messageId;
        resolve(userMessage);
      }
    });
  });
};

export const fetchMessages = (
  payload: IActiveChannel
): Promise<IMapMessage | void> => {
  const { listQuery: messageListQuery } = payload;

  messageListQuery.limit = FETCH_MSG_LIMIT;
  messageListQuery.reverse = false;

  const processMessages = (messages: UserMessage[]) => {
    const formated = (messages || []).reduce((acc, current: UserMessage): {
      [key: string]: UserMessage;
    } => {
      return {
        ...acc,
        [current.messageId]: current,
      };
    }, {});
    return Map(formated);
  };

  return new Promise((resolve, eject) => {
    messageListQuery.load((messages, error) => {
      if (error) {
        eject(error);
      } else {
        const msgs = processMessages(messages as UserMessage[]);
        resolve(msgs as IMapMessage);
      }
    });
  });
};

export * from "./messagesApi";
