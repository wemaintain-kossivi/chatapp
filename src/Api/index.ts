import SendBird, {
  OpenChannel,
  PreviousMessageListQuery,
  User,
  UserMessage,
  UserMessageParams,
} from "sendbird";

import { Map } from "immutable";

import SEND_BIRD_CONFIG from "../../senbird.config";
import { IActiveChannel, IMap, IMapChannel, ISendPayload } from "../types";

export const SB = new SendBird({ appId: SEND_BIRD_CONFIG.APP_ID });

export const UNIQUE_HANDLER_ID = "CHAT_APP_UNIQUE_HANDLER_ID";

//channel handler
export const channelEventHandler = new SB.ChannelHandler();
//SB.addChannelHandler(UNIQUE_HANDLER_ID, channelEventHandler);
export const removeHandlerLister = () => {
  SB.removeChannelHandler(UNIQUE_HANDLER_ID);
};

//login
export const loginSendBird = async (userId: string): Promise<User> => {
  const user: User = await SB.connect(userId);
  return user;
};

export const logOutSendBird = async () => {
  await SB.disconnect();
};

export * from "./channelsApi";
export * from "./messagesApi";
export * from "./usersApi";
