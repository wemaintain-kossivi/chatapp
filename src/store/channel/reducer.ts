import { fromJS } from "immutable";

import {
  IError,
  IAction,
  IChannel,
  IActiveChannel,
  IMapChannel,
} from "../../types";
import ACTION_TYPES from "../actionTypes";
import { uniqueMerge } from "../../helpers";

const {
  CHANNEL_FETCH,
  CHANNEL_FETCH_SUCCESS,
  CHANNEL_FETCH_ERROR,
  CHANNEL_SELECT,
  CHANNEL_RESET,
  PURGE,
  CHANNEL_SELECT_SUCCESS,
  CHANNEL_SELECT_ERROR,
  CHANNEL_EDIT,
  CHANNEL_EDIT_SUCCESS,
  CHANNEL_EDIT_ERROR,
} = ACTION_TYPES;

const initalState: IChannel = {
  isFetching: false,
  isSelecting: false,
  isEditing: false,
  map: fromJS({}),
  activeChannel: undefined,
  error: undefined,
};

const _fetch = (state: IChannel, action: IAction<undefined>): IChannel => {
  return { ...state, isFetching: true };
};
const _fetchSuccess = (
  state: IChannel,
  action: IAction<IMapChannel>
): IChannel => {
  const { payload } = action;

  return {
    ...state,
    map: payload,
    isFetching: false,
  };
};

const _fetchError = (state: IChannel, action: IAction<IError>): IChannel => {
  const { payload } = action;
  return { ...state, error: payload, isFetching: false };
};

const _select = (state: IChannel, action: IAction<string>): IChannel => {
  return { ...state, isSelecting: true };
};
const _selectSuccess = (
  state: IChannel,
  action: IAction<IActiveChannel>
): IChannel => {
  const { payload } = action;
  return { ...state, activeChannel: payload, isSelecting: false };
};
const _selectError = (state: IChannel, action: IAction<IError>): IChannel => {
  return { ...state, isSelecting: false };
};

const _edit = (state: IChannel, action: IAction<undefined>): IChannel => {
  return { ...state, isEditing: true };
};
const _editSuccess = (
  state: IChannel,
  action: IAction<undefined>
): IChannel => {
  return {
    ...state,
    isEditing: false,
  };
};

const _editError = (state: IChannel, action: IAction<IError>): IChannel => {
  const { payload } = action;
  return { ...state, error: payload, isEditing: false };
};

const reducer = (state = initalState, action: IAction<any>): IChannel => {
  const type: string = action.type;
  switch (type) {
    case CHANNEL_FETCH:
      return _fetch(state, action);
    case CHANNEL_FETCH_SUCCESS:
      return _fetchSuccess(state, action);
    case CHANNEL_FETCH_ERROR:
      return _fetchError(state, action);
    case CHANNEL_SELECT:
      return _select(state, action);
    case CHANNEL_SELECT_SUCCESS:
      return _selectSuccess(state, action);
    case CHANNEL_SELECT_ERROR:
      return _selectError(state, action);
    case CHANNEL_EDIT:
      return _edit(state, action);
    case CHANNEL_EDIT_SUCCESS:
      return _editSuccess(state, action);
    case CHANNEL_EDIT_ERROR:
      return _editError(state, action);
    case PURGE:
    case CHANNEL_RESET:
      return initalState;
    default:
      return state;
  }
};

export default reducer;
