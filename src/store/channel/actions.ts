import { OpenChannel } from "sendbird";

import {
  IAction,
  IActiveChannel,
  IChannelAction,
  IError,
  IMapChannel,
} from "../../types";
import ACTIONS_TYPES from "../actionTypes";
const {
  CHANNEL_FETCH_SUCCESS,
  CHANNEL_SELECT,
  CHANNEL_FETCH_ERROR,
  CHANNEL_FETCH,
  CHANNEL_SELECT_SUCCESS,
  CHANNEL_SELECT_ERROR,
  CHANNEL_EDIT,
  CHANNEL_EDIT_SUCCESS,
  CHANNEL_EDIT_ERROR,
} = ACTIONS_TYPES;

export const channelFetch = (payload?: string): IAction<undefined | string> => {
  return {
    type: CHANNEL_FETCH,
    payload: payload,
  };
};

export const channelFetchSuccess = (
  channels: IMapChannel
): IAction<IMapChannel> => {
  return {
    type: CHANNEL_FETCH_SUCCESS,
    payload: channels,
  };
};

export const channelFetchError = (error: IError): IAction<IError> => {
  return {
    type: CHANNEL_FETCH_ERROR,
    payload: error,
  };
};

export const channelSelect = (payload: OpenChannel): IAction<OpenChannel> => {
  return {
    type: CHANNEL_SELECT,
    payload: payload,
  };
};
export const channelSelectSucsess = (
  payload: IActiveChannel
): IAction<IActiveChannel> => {
  return {
    type: CHANNEL_SELECT_SUCCESS,
    payload: payload,
  };
};

export const channelSelectError = (error: IError): IAction<IError> => {
  return {
    type: CHANNEL_SELECT_ERROR,
    payload: error,
  };
};

export const channelEdit = (
  payload: IChannelAction
): IAction<IChannelAction> => {
  return {
    type: CHANNEL_EDIT,
    payload: payload,
  };
};

export const channelEditSucsess = (
  payload: OpenChannel
): IAction<OpenChannel> => {
  return {
    type: CHANNEL_EDIT_SUCCESS,
    payload: payload,
  };
};
export const channelEditError = (error: IError): IAction<IError> => {
  return {
    type: CHANNEL_EDIT_ERROR,
    payload: error,
  };
};
