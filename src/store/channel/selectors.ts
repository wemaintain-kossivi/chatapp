import { OpenChannel } from "sendbird";
import { channelTypeLabel } from "../../helpers";
import { IActiveChannel } from "../../types";
import { ICrispyListItem } from "../../UI";
import { IStoreState } from "../index";

export const channelsList = (
  state: IStoreState
): ICrispyListItem<OpenChannel>[] => {
  const rawData: OpenChannel[] = [...(state.channelStore.map?.values() || [])];

  return rawData
    .map((itemUI: OpenChannel) => ({
      itemId: itemUI.url,
      title: itemUI.name,
      subTitle: channelTypeLabel(itemUI.isOpenChannel()),
      avatar: itemUI.coverUrl,
      raw: itemUI,
    }))
    .sort((a, b): number => (a.raw.createdAt < b.raw.createdAt ? 1 : -1));
};

export const selectedChannel = (
  state: IStoreState
): IActiveChannel | undefined => {
  const activeItem = state.channelStore.activeChannel;
  const freshChannel = state.channelStore.map?.get(
    activeItem?.channel.url as string
  );
  return {
    ...activeItem,
    channel: freshChannel,
  };
};

export const channelIsFetching = (state: IStoreState): boolean => {
  const isFetching = state.channelStore.isFetching;
  return !!isFetching;
};
