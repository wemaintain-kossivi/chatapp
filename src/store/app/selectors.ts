import { fromJS } from "immutable";
import { channelTypeLabel } from "../../helpers";
import { IMapNotif, INotification } from "../../types";
import { ICrispyListItem } from "../../UI";
import { IStoreState } from "../index";

export const appIsOffline = (state: IStoreState): boolean => {
  return !state.appStore.isOnline;
};

export const appNotifs = (
  state: IStoreState
): ICrispyListItem<INotification>[] => {
  const rawData: INotification[] = [...(state.appStore.notifs?.values() || [])];

  return rawData.map((itemUI: INotification) => ({
    itemId: itemUI.id,
    title: itemUI.title,
    subTitle: channelTypeLabel(itemUI.createdAt + ""),
    raw: itemUI,
  }));
};

export const appHasNotification = (state: IStoreState): boolean => {
  return !!state.appStore.hasNewNotif;
};
