import {
  IAction,
  INotification,
  INotificationBase,
  NOTIF_TYPE,
} from "../../types";
import ACTIONS_TYPES from "../actionTypes";
import uuid from "react-native-uuid";

const {
  APP_ONLINE,
  APP_OFFLINE,
  PURGE,
  APP_NOTIF_ADD,
  APP_NOTIF_CLEAR,
  APP_NOTIF_MARK_ALL_AS_READ,
} = ACTIONS_TYPES;

export const purgeApp = (): IAction => {
  return {
    type: PURGE,
  };
};

export const refreshOnlineStatus = (status: boolean) => {
  return {
    type: !status ? APP_OFFLINE : APP_ONLINE,
  };
};

export const addNotif = (
  payload: INotificationBase
): IAction<INotificationBase> => {
  const completeObj: INotification = {
    ...(payload as INotificationBase),
    id: uuid.v4() as string,
    createdAt: `${Date.now()}`,
  };
  return {
    type: APP_NOTIF_ADD,
    payload: completeObj,
  };
};

export const newUserNotif = () => {
  const payload: INotificationBase = {
    type: NOTIF_TYPE.LOGGED_ON,
    title: "New User notif",
  };
  return addNotif(payload);
};

export const markALLNotifsAsRead = (): IAction => {
  return {
    type: APP_NOTIF_MARK_ALL_AS_READ,
  };
};

export const clearAllNotifs = (): IAction => {
  return {
    type: APP_NOTIF_CLEAR,
  };
};
