import { User } from "sendbird";
import { fromJS } from "immutable";

import {
  IError,
  IAction,
  IApp,
  INotification,
  INotificationBase,
} from "../../types";
import ACTION_TYPES from "../actionTypes";

const {
  APP_OFFLINE,
  APP_ONLINE,
  PURGE,
  SET_MSG_LIST_QUERY,
  APP_NOTIF_ADD,
  APP_NOTIF_CLEAR,
  APP_NOTIF_MARK_ALL_AS_READ,
} = ACTION_TYPES;

const initalState: IApp = {
  isOnline: true,
  notifs: fromJS({}),
  hasNewNotif: false,
};

const _isOffline = (state: IApp): IApp => {
  return { ...state, isOnline: false };
};
const _isOnline = (state: IApp): IApp => {
  return { ...state, isOnline: true };
};
const _addNotif = (state: IApp, action: IAction<INotification>): IApp => {
  const { payload } = action;

  return {
    ...state,
    notifs: state.notifs?.set(payload?.id as string, payload),
    hasNewNotif: true,
  };
};

const _marAllNotifsAsRead = (state: IApp): IApp => {
  return { ...state, hasNewNotif: false };
};
const _clearAllNotifs = (state: IApp): IApp => {
  return { ...state, hasNewNotif: false, notifs: fromJS({}) };
};

const reducer = (state = initalState, action: IAction<any>): IApp => {
  const type: string = action.type;
  switch (type) {
    case APP_ONLINE:
      return _isOnline(state);
    case APP_OFFLINE:
      return _isOffline(state);
    case APP_NOTIF_ADD:
      return _addNotif(state, action);
    case APP_NOTIF_MARK_ALL_AS_READ:
      return _marAllNotifsAsRead(state);
    case APP_NOTIF_CLEAR:
      return _clearAllNotifs(state);
    case PURGE:
      return initalState;
    default:
      return state;
  }
};

export default reducer;
