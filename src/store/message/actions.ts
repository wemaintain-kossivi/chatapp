import { OpenChannel, User, UserMessage } from "sendbird";

import {
  IAction,
  IActiveChannel,
  IError,
  IMap,
  ISendPayload,
} from "../../types";
import ACTIONS_TYPES from "../actionTypes";
const {
  MESSAGE_FETCH,
  MESSAGE_FETCH_SUCCESS,
  MESSAGE_FETCH_ERROR,
  MESSAGE_SEND,
  MESSAGE_SEND_SUCCESS,
  MESSAGE_SEND_ERROR,
  MESSAGE_PURGE,
  MESSAGE_ADD_RECIEVED,
} = ACTIONS_TYPES;

export const messageFetch = (): IAction<undefined> => {
  return {
    type: MESSAGE_FETCH,
  };
};
export const messagePurge = (): IAction<undefined> => {
  return {
    type: MESSAGE_PURGE,
  };
};
export const addMessageRecieved = (
  payload: UserMessage
): IAction<UserMessage> => {
  return {
    type: MESSAGE_ADD_RECIEVED,
    payload: payload,
  };
};

export const messageFetchSuccess = (
  messages: IMap<UserMessage>
): IAction<IMap<UserMessage>> => {
  return {
    type: MESSAGE_FETCH_SUCCESS,
    payload: messages,
  };
};

export const messageFetchError = (error: IError): IAction<IError> => {
  return {
    type: MESSAGE_FETCH_ERROR,
    payload: error,
  };
};

export const messageSend = (paylaod: ISendPayload): IAction<ISendPayload> => {
  return {
    type: MESSAGE_SEND,
    payload: paylaod,
  };
};

export const messageSendSuccess = (
  payload: UserMessage
): IAction<UserMessage> => {
  return {
    type: MESSAGE_SEND_SUCCESS,
    payload: payload,
  };
};

export const messageSendError = (error: IError): IAction<IError> => {
  return {
    type: MESSAGE_SEND_ERROR,
    payload: error,
  };
};
