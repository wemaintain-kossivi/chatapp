import { UserMessage } from "sendbird";
import { fromJS, mergeDeep } from "immutable";

import { IError, IAction, IMessage, IMapMessage } from "../../types";
import ACTION_TYPES from "../actionTypes";
import { State } from "react-native-gesture-handler";

const {
  MESSAGE_FETCH,
  MESSAGE_SEND,
  MESSAGE_SEND_SUCCESS,
  MESSAGE_SEND_ERROR,
  MESSAGE_FETCH_ERROR,
  MESSAGE_FETCH_SUCCESS,
  PURGE,
  MESSAGE_PURGE,
  MESSAGE_ADD_RECIEVED,
} = ACTION_TYPES;

const initalState: IMessage = {
  isFetching: false,
  isSending: false,
  error: undefined,
  sendError: undefined,
  map: fromJS({}),
};

const _fetch = (state: IMessage): IMessage => {
  return { ...state, isFetching: true };
};

const _fetchSuccess = (
  state: IMessage,
  action: IAction<IMapMessage>
): IMessage => {
  const { payload } = action;
  return {
    ...state,
    map: mergeDeep(state.map, payload as IMapMessage),
    isFetching: false,
  };
};

const _fetchError = (state: IMessage, action: IAction<IError>): IMessage => {
  const { payload } = action;
  return {
    ...state,
    error: payload,
    isFetching: false,
  };
};

const _send = (state: IMessage): IMessage => {
  return { ...state, isSending: true };
};

const _sendSuccess = (
  state: IMessage,
  action: IAction<UserMessage>
): IMessage => {
  const { payload } = action;
  return {
    ...state,
    map: state.map.set(payload?.messageId, payload),
    isSending: false,
  };
};
const _recievedMessage = (
  state: IMessage,
  action: IAction<UserMessage>
): IMessage => {
  const { payload } = action;
  return {
    ...state,
    map: state.map.set(payload?.messageId, payload),
    isSending: false,
  };
};

const _sendError = (state: IMessage, action: IAction<IError>): IMessage => {
  const { payload } = action;
  return { ...state, error: payload, isSending: false };
};

const _messagePurge = (
  sate: IMessage,
  action: IAction<undefined>
): IMessage => {
  return {
    ...State,
    map: fromJS({}),
  };
};

const reducer = (state = initalState, action: IAction<any>): IMessage => {
  const type: string = action.type;
  switch (type) {
    case MESSAGE_FETCH:
      return _fetch(state);
    case MESSAGE_FETCH_SUCCESS:
      return _fetchSuccess(state, action);
    case MESSAGE_FETCH_ERROR:
      return _fetchError(state, action);
    case MESSAGE_SEND:
      return _send(state);
    case MESSAGE_SEND_SUCCESS:
      return _sendSuccess(state, action);
    case MESSAGE_SEND_ERROR:
      return _sendError(state, action);
    case MESSAGE_PURGE:
      return _messagePurge(state, action);
    case MESSAGE_ADD_RECIEVED:
      return _recievedMessage(state, action);
    case PURGE:
      return initalState;
    default:
      return state;
  }
};

export default reducer;
