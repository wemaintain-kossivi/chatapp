import { UserMessage } from "sendbird";
import { IMessageUI } from "../../types";
import { IStoreState } from "../index";

export const messagesList = (state: IStoreState): IMessageUI[] => {
  const rawData: UserMessage[] = [...state.messageStore.map.values()];
  return rawData
    .map((itemUI: UserMessage) => ({
      itemId: itemUI.messageId + "",
      sender: itemUI.sender.userId || itemUI.sender.nickname,
      content: itemUI.message,
      avatar: itemUI.sender.nickname,
      raw: itemUI,
    }))
    .sort((a, b): number => (a.raw.createdAt < b.raw.createdAt ? 1 : -1));
};

export const messageIsFetching = (state: IStoreState): boolean => {
  const isFetching = state.messageStore.isFetching;
  return !!isFetching;
};
