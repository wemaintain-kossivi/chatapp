import { IStoreState } from "../index";
import { IAuth, IError } from "../../types";
import { User } from "sendbird";

export const isLogOn = (state: IStoreState): boolean => {
  const user = state.authStore.user;
  return !!user;
};
export const authIsFetching = (state: IStoreState): boolean => {
  const isFetching = state.authStore.isFetching;
  return !!isFetching;
};
export const authError = (state: IStoreState): IError | undefined => {
  return state.authStore.error;
};
export const authUser = (state: IStoreState): User | undefined => {
  return state.authStore.user;
};
export const authUserAvatarUrl = (state: IStoreState): string | undefined => {
  return state.authStore.user?.profileUrl;
};
