import { OpenChannel, User } from "sendbird";

import { IAction, IAuth, IError } from "../../types";
import ACTIONS_TYPES from "../actionTypes";
const { LOGIN, LOGIN_SUCCESS, LOGIN_ERROR, LOGOUT, LOGIN_ABORT, JOIN_CHANNEL } =
  ACTIONS_TYPES;

export const login = (pseudo: string): IAction<String> => {
  return {
    type: LOGIN,
    payload: pseudo.toLowerCase(),
  };
};
export const loginSuccess = (user: User): IAction<User> => {
  return {
    type: LOGIN_SUCCESS,
    payload: user,
  };
};
export const loginError = (error: IError): IAction<IError> => {
  return {
    type: LOGIN_ERROR,
    payload: error,
  };
};
export const logOut = (): IAction<any> => {
  return {
    type: LOGOUT,
  };
};
export const loginAbort = (): IAction<any> => {
  return {
    type: LOGIN_ABORT,
  };
};

export const joinChannel = (payload: OpenChannel): IAction<OpenChannel> => {
  return {
    type: JOIN_CHANNEL,
    payload: payload,
  };
};
