import { User } from "sendbird";
import { Map } from "immutable";

import { IError, IAction, IAuth } from "../../types";
import ACTION_TYPES from "../actionTypes";

const { LOGIN_SUCCESS, LOGIN_ERROR, LOGOUT, LOGIN_ABORT, LOGIN, PURGE } =
  ACTION_TYPES;

const initalState: IAuth = {
  user: undefined,
  error: undefined,
  isFetching: false,
};

const _login = (state: IAuth, action: IAction<any>): IAuth => {
  return { ...state, isFetching: true };
};

const _loginSuccess = (state: IAuth, action: IAction<User>): IAuth => {
  const { payload } = action;
  return {
    ...state,
    user: payload,
    isFetching: false,
  };
};

const _loginError = (state: IAuth, action: IAction<IError>): IAuth => {
  const { payload } = action;
  return { ...state, error: payload, user: undefined, isFetching: false };
};

const _logOut = (state: IAuth, action: IAction<any>) => {
  return {
    ...state,
    error: undefined,
    user: undefined,
    isFetching: false,
  };
};

const _loginAbort = (state: IAuth, action: IAction<any>): IAuth => {
  return {
    ...state,
    error: undefined,
    user: undefined,
    isFetching: false,
  };
};

const reducer = (state = initalState, action: IAction<any>): IAuth => {
  const type: string = action.type;
  switch (type) {
    case LOGIN:
      return _login(state, action);
    case LOGIN_SUCCESS:
      return _loginSuccess(state, action);
    case LOGIN_ERROR:
      return _loginError(state, action);
    case LOGOUT:
      return _logOut(state, action);
    case LOGIN_ABORT:
      return _loginAbort(state, action);
    case PURGE:
      return initalState;
    default:
      return state;
  }
};

export default reducer;
