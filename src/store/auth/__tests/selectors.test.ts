import SendBird, { User } from "sendbird";
import { ERROR_CODE, IAuth, IError } from "../../../types";
import { isLogOn, authIsFetching, authError } from "../../selectors";
import { IStoreState } from "../../index";

describe("auth selectors", () => {
  const state: IStoreState = {
    authStore: {
      error: undefined,
      user: undefined,
      isFetching: undefined,
    },
  };
  const stateLogin: IStoreState = {
    authStore: {
      error: undefined,
      user: {},
    },
  };

  it("isLogon tobeFalsy", () => {
    expect(isLogOn(state)).toBeFalsy();
  });
  it("isLogon tobeTruthy", () => {
    expect(isLogOn(stateLogin)).toBeTruthy();
  });

  it("authIsFetching falsy", () => {
    const state: IStoreState = {
      authStore: {
        isFetching: false,
      },
    };
    expect(authIsFetching(state)).toBeFalsy();
  });

  it("authIsFetching tobeTruthy", () => {
    const state: IStoreState = {
      authStore: {
        isFetching: true,
      },
    };
    expect(authIsFetching(state)).toBeTruthy();
  });

  it("authError", () => {
    const error: IError = {
      code: ERROR_CODE.DEFAULT,
      msg: "trial expired",
    };
    const state: IStoreState = {
      authStore: {
        error: error,
      },
    };
    const expectedError = {
      code: ERROR_CODE.DEFAULT,
      msg: "trial expired",
    };
    expect(authError(state)).toEqual(expectedError);
  });
});
