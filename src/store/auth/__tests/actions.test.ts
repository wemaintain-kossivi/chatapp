import SendBird, { User } from "sendbird";

import { ERROR_CODE, IAuth, IError } from "../../../types";
import ACTIONS_TYPES from "../../actionTypes";
import {
  login,
  loginSuccess,
  loginError,
  loginAbort,
  logOut,
} from "../actions";

const { LOGIN, LOGIN_SUCCESS, LOGIN_ERROR, LOGOUT, LOGIN_ABORT } =
  ACTIONS_TYPES;

describe("auth actions creators", () => {
  const state = {
    error: undefined,
    user: undefined,
  };

  it(" action login", () => {
    const expectedAction = {
      type: LOGIN,
      payload: "pseudo",
    };
    expect(login("PseuDO")).toEqual(expectedAction);
  });

  it(" action loginSuccess", () => {
    const expectedAction = {
      type: LOGIN_SUCCESS,
      payload: {},
    };
    expect(loginSuccess({})).toEqual(expectedAction);
  });

  it(" action loginError", () => {
    const expectedAction = {
      type: LOGIN_ERROR,
      payload: {},
    };
    expect(loginError({})).toEqual(expectedAction);
  });

  it(" action logout", () => {
    const expectedAction = {
      type: LOGOUT,
    };
    expect(logOut()).toEqual(expectedAction);
  });

  it(" action loginAbort", () => {
    const expectedAction = {
      type: LOGIN_ABORT,
    };
    expect(loginAbort()).toEqual(expectedAction);
  });
});
