import SendBird, { User } from "sendbird";

import { ERROR_CODE, IAuth, IError } from "../../../types";
import ACTIONS_TYPES from "../../actionTypes";
import reducer from "../reducer";

const { LOGIN, LOGIN_SUCCESS, LOGIN_ERROR, LOGOUT, LOGIN_ABORT } =
  ACTIONS_TYPES;

describe("auth reducer", () => {
  const initialState = {
    error: undefined,
    user: undefined,
    isFetching: false,
  };

  it("DEFAULT_REDUCER", () => {
    const action = {
      type: "dummy",
    };

    expect(reducer(undefined, action)).toEqual(initialState);
  });

  it("LOGIN", () => {
    const action = {
      type: LOGIN,
      payload: "pseudo",
    };
    const expectedState = {
      error: undefined,
      user: undefined,
      isFetching: true,
    };
    expect(reducer(initialState, action)).toEqual(expectedState);
  });

  it("LOGIN_SUCCESS", () => {
    const user: User = {
      userId: "pseudo",
    };
    const action = {
      type: LOGIN_SUCCESS,
      payload: user,
    };

    const expectedState = {
      error: undefined,
      user: user,
      isFetching: false,
    };
    expect(reducer(initialState, action)).toEqual(expectedState);
  });

  it("LOGIN_ERROR", () => {
    const user: User = {
      userId: "pseudo",
    };
    const currentState: IAuth = {
      error: undefined,
      user: user,
    };
    const error: IError = {
      code: ERROR_CODE.DEFAULT,
      msg: "trial expired",
    };

    const action = {
      type: LOGIN_ERROR,
      payload: error,
    };
    const expectedState = {
      error: error,
      user: undefined,
      isFetching: false,
    };
    expect(reducer(currentState, action)).toEqual(expectedState);
  });

  it("LOGOUT", () => {
    const user: User = {
      userId: "pseudo",
    };
    const currentState: IAuth = {
      error: undefined,
      isFetching: undefined,
      user: user,
    };

    const action = {
      type: LOGOUT,
    };
    const expectedState = {
      error: undefined,
      user: undefined,
      isFetching: false,
    };
    expect(reducer(currentState, action)).toEqual(expectedState);
  });

  it("LOGIN_ABORT", () => {
    const currentState: IAuth = {
      error: undefined,
      isFetching: true,
      user: undefined,
    };

    const action = {
      type: LOGIN_ABORT,
    };
    const expectedState = {
      error: undefined,
      user: undefined,
      isFetching: false,
    };
    expect(reducer(currentState, action)).toEqual(expectedState);
  });
});
