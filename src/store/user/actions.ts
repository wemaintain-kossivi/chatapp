import { OpenChannel, User } from "sendbird";

import { IAction, IAuth, IChannel, IError, IMap, IMapUser } from "../../types";
import ACTIONS_TYPES from "../actionTypes";
const { USER_FETCH, USER_FETCH_SUCCESS, USER_FETCH_ERROR } = ACTIONS_TYPES;

export const usersFetch = (): IAction<undefined> => {
  return {
    type: USER_FETCH,
  };
};

export const usersFetchSuccess = (payload: IMapUser): IAction<IMapUser> => {
  return {
    type: USER_FETCH_SUCCESS,
    payload: payload,
  };
};

export const userFetchError = (error: IError): IAction<IError> => {
  return {
    type: USER_FETCH_ERROR,
    payload: error,
  };
};
