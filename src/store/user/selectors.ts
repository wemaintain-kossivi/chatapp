import { OpenChannel, User } from "sendbird";
import { ICrispyListItem } from "../../UI";
import { IStoreState } from "../index";
import { lastSeenLabel } from "../../helpers";
import { IMapUser } from "../../types";

export const usersList = (state: IStoreState): ICrispyListItem<User>[] => {
  const rawData: User[] = [...state.userStore.map.values()];
  return rawData.map((itemUI: User) => ({
    itemId: itemUI.userId,
    title: itemUI.nickname || itemUI.userId,
    subTitle: lastSeenLabel(itemUI.lastSeenAt),
    avatar: itemUI.profileUrl,
    raw: itemUI,
    hasBadge: itemUI.lastSeenAt == "0",
  }));
};

export const userIsFetching = (state: IStoreState): boolean => {
  const isFetching = state.userStore.isFetching;
  return !!isFetching;
};
