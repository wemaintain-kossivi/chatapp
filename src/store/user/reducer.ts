import { OpenChannel, User } from "sendbird";
import { fromJS } from "immutable";

import { IError, IAction, IUser, IMapUser } from "../../types";
import ACTION_TYPES from "../actionTypes";

const { USER_FETCH, USER_FETCH_SUCCESS, USER_FETCH_ERROR, USER_RESET, PURGE } =
  ACTION_TYPES;

const initalState: IUser = {
  isFetching: false,
  map: fromJS({}),
  error: undefined,
};

const _fetch = (state: IUser, action: IAction<undefined>): IUser => {
  return { ...state, isFetching: true };
};
const _fetchSuccess = (state: IUser, action: IAction<IMapUser>): IUser => {
  const { payload } = action;
  return {
    ...state,
    map: state.map.concat(payload as IMapUser),
    isFetching: false,
  };
};

const _fetchError = (state: IUser, action: IAction<IError>): IUser => {
  const { payload } = action;
  return { ...state, error: payload, isFetching: false };
};

const reducer = (state = initalState, action: IAction<any>): IUser => {
  const type: string = action.type;
  switch (type) {
    case USER_FETCH:
      return _fetch(state, action);
    case USER_FETCH_SUCCESS:
      return _fetchSuccess(state, action);
    case USER_FETCH_ERROR:
      return _fetchError(state, action);
    case PURGE:
    case USER_RESET:
      return state;
    default:
      return state;
  }
};

export default reducer;
