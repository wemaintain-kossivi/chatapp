import { createStore, combineReducers, applyMiddleware } from "redux";
import createSagaMidleware from "redux-saga";
import { persistStore, persistReducer } from "redux-persist";
import { AsyncStorage } from "react-native";

import appReducer from "./app/reducer";
import authReducer from "./auth/reducer";
import channelReducer from "./channel/reducer";
import userReducer from "./user/reducer";
import messageReducer from "./message/reducer";
import sagas from "../sagas";

//persist and combine reducers
const rootPersistConfig = {
  key: "root",
  storage: AsyncStorage,
  whitelist: ["authStore"],
};

const authPersistConfig = {
  key: "authStore",
  storage: AsyncStorage,
  blacklist: ["isFetching", "error"],
};
export const rootReducer = combineReducers({
  appStore: appReducer,
  channelStore: channelReducer,
  userStore: userReducer,
  messageStore: messageReducer,
  authStore: persistReducer(authPersistConfig, authReducer),
});
const persistedReducer = persistReducer(rootPersistConfig, rootReducer);

// saga and store instance
const sagaMiddleware = createSagaMidleware();
const store = createStore(persistedReducer, applyMiddleware(sagaMiddleware));

export type IStoreState = ReturnType<typeof store.getState>;
export const persistor = persistStore(store);
export default store;

sagaMiddleware.run(sagas);
