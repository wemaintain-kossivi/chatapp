import { Ionicons } from "@expo/vector-icons";

import ContainerUI from "./Container";
import BigHeaderUI from "./BigHeader";
import SplitterUI from "./Splitter";
import ListItemUI from "./ListItem";
import HeaderTitleUI from "./HeaderTitle";
import KeyboardSafeAreaUI from "./KeyboardSafeArea";
import CrispyListUI from "./CrispyList";
import CrispyAvatarUI from "./CrispyAvatar";
import HeaderActionsUI from "./HeaderActions";
import EmptyComponentUI from "./EmptyComponent";

export const Container = ContainerUI;
export const BigHeader = BigHeaderUI;
export const Splitter = SplitterUI;
export const ListItem = ListItemUI;
export const HeaderTitle = HeaderTitleUI;
export const KeyboardSafeArea = KeyboardSafeAreaUI;
export const CrispyList = CrispyListUI;
export const CrispyAvatar = CrispyAvatarUI;
export const HeaderActions = HeaderActionsUI;
export const EmptyComponent = EmptyComponentUI;

export type IoniconType = keyof typeof Ionicons.glyphMap;
export interface IHeaderIcon {
  name: IoniconType;
  color?: string;
  onPress?: Function;
  disabled?: boolean;
}
export interface ICrispyListItem<T = undefined> {
  itemId: string;
  title?: string;
  subTitle?: string;
  avatar?: string;
  hasBadge?: boolean;
  badgeStatus?: any;
  raw?: T;
}
