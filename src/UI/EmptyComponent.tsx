import * as React from "react";
import { StyleSheet, Pressable } from "react-native";
import { Text, useTheme, Icon } from "react-native-elements";

import { Container, IoniconType } from ".";

interface IProps {
  title?: string;
  text?: string;
  icon?: IoniconType;
}

const Component: React.FC<IProps> = (props) => {
  const { theme } = useTheme();
  const { title, text, icon } = props;

  return (
    <Container flex style={{ backgroundColor: "#eeeeee", height: "100%" }}>
      <Icon
        name={icon || "file-tray-outline"}
        iconStyle={{
          color: theme.colors?.grey4,
        }}
        size={theme.fontSizes?.xxLarge}
      />
      {title && (
        <Text h4 style={{ color: theme.colors?.grey4, textAlign: "center" }}>
          {title}
        </Text>
      )}
      {text && (
        <Text style={{ color: theme.colors?.grey2, textAlign: "center" }}>
          {text}
        </Text>
      )}
    </Container>
  );
};

const styles = StyleSheet.create({
  container: {},
});

export default Component;
