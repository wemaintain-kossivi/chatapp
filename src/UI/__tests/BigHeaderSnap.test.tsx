import * as React from "react";
import renderer from "react-test-renderer";

import BigHeader from "../BigHeader";

describe(" BigHeader Component snap", () => {
  it("BigHeader default", () => {
    const json = renderer
      .create(<BigHeader bgIcon="home" bgColor="primary" />)
      .toJSON();
    expect(json).toMatchSnapshot();
  });

  it("Container  with title", () => {
    const json = renderer
      .create(<BigHeader bgIcon="home" bgColor="primary" title="title" />)
      .toJSON();
    expect(json).toMatchSnapshot();
  });
  it("Container  bgColor secondary", () => {
    const json = renderer
      .create(<BigHeader bgIcon="home" bgColor="secondary" />)
      .toJSON();
    expect(json).toMatchSnapshot();
  });
});
