import * as React from "react";
import renderer from "react-test-renderer";

import Container from "../Container";

describe(" Container Componenet snap", () => {
  it("Container default", () => {
    const json = renderer.create(<Container />).toJSON();
    expect(json).toMatchSnapshot();
  });

  it("Container  nopadding", () => {
    const json = renderer.create(<Container noPadding={true} />).toJSON();
    expect(json).toMatchSnapshot();
  });

  it("Container  noPadding == false", () => {
    const json = renderer.create(<Container noPadding={false} />).toJSON();
    expect(json).toMatchSnapshot();
  });

  it("Container  flex ", () => {
    const json = renderer.create(<Container noPadding={true} />).toJSON();
    expect(json).toMatchSnapshot();
  });
  it("Container  flex == false ", () => {
    const json = renderer.create(<Container noPadding={false} />).toJSON();
    expect(json).toMatchSnapshot();
  });
});
