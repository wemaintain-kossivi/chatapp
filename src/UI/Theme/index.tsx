import { IFontSize, Spacing, Theme } from "react-native-elements";

const defaultSpacing: Spacing = {
  small: 5,
  medium: 10,
  large: 15,
  xLarge: 30,
};
const fontSizes: IFontSize = {
  xSmall: 12,
  small: 15,
  medium: 18,
  large: 20,
  xLarge: 25,
  xxLarge: 60,
};

const textStyle = {
  fontFamily: "noto-regular",
  color: "#444444",
};
const titleStyle = {
  fontFamily: "noto-bold",
};

const theme: Theme = {
  fontSizes: fontSizes,
  Spacing: defaultSpacing,
  Text: {
    style: {
      ...textStyle,
    },
    h1Style: {
      ...titleStyle,
    },
    h2Style: {
      ...titleStyle,
      textTransform: "uppercase",
    },
    h3Style: {
      ...titleStyle,
    },
  },
  Button: {
    titleStyle: {
      ...textStyle,
    },
  },
  FAB: {
    titleStyle: {
      ...titleStyle,
    },
  },
  ListItemTitle: {
    style: {
      ...titleStyle,
      fontWeight: "bold",
    },
  },
  Input: {
    style: {
      ...textStyle,
    },
    underlineColorAndroid: "transparent",
    inputContainerStyle: {
      borderWidth: 1,
      borderRadius: 30,
      borderColor: "#dddddd",
      paddingLeft: 5,
      paddingRight: 5,
      backgroundColor: "#f2f2f2",
    },
  },
  colors: {
    primary: "#272C6C",
    secondary: "#f1653c",
  },
  Divider: {
    style: {
      width: "100%",
    },
  },
  Header: {
    backgroundColor: "#ffffff",
  },
  Icon: {
    type: "ionicon",
  },
};

export default theme;
