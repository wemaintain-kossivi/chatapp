import * as React from "react";
import { StyleSheet, TextStyle, View } from "react-native";
import { useTheme, Avatar, ListItem, Badge } from "react-native-elements";

import { Container, CrispyAvatar, IoniconType } from "./index";

interface Iprops {
  avatar?: string;
  title?: string;
  subTitle?: string;
  onPress?: Function;
  showChevron?: boolean;
  rightComponent?: React.ReactNode;
  iconSize?: undefined | "small" | "medium" | "large" | "xlarge";
  avatarIcon?: IoniconType;
  subTitleStyle?: TextStyle;
  hasBadge?: boolean;
  badgeStatus?: "warning" | "primary" | "success" | "error" | undefined;
}
const Item: React.FC<Iprops> = (props) => {
  const { useState } = React;
  const {
    avatar,
    title,
    subTitle,
    onPress,
    showChevron,
    rightComponent,
    iconSize,
    avatarIcon,
    subTitleStyle,
    hasBadge,
    badgeStatus,
  } = props;

  const { theme } = useTheme();

  return (
    <ListItem
      bottomDivider
      containerStyle={{ width: "100%" }}
      onPress={onPress as any}
    >
      <View>
        <CrispyAvatar
          avatarUrl={avatar}
          size={iconSize || "small"}
          icon={avatarIcon}
        />
        {hasBadge && (
          <Badge
            status={badgeStatus || "success"}
            containerStyle={{ position: "absolute", bottom: 0, right: 0 }}
          />
        )}
      </View>

      <Container noPadding flex style={styles.listRow}>
        <ListItem.Content style={styles.content}>
          <ListItem.Title style={{ fontSize: theme.fontSizes?.small }}>
            {title}
          </ListItem.Title>
          <ListItem.Subtitle numberOfLines={1} style={subTitleStyle}>
            {subTitle}
          </ListItem.Subtitle>
        </ListItem.Content>
        {!!rightComponent && (
          <Container noPadding style={styles.rightContainer}>
            {rightComponent}
          </Container>
        )}
      </Container>

      {!!showChevron && <ListItem.Chevron />}
    </ListItem>
  );
};
const styles = StyleSheet.create({
  listRow: {
    flex: 1,
    flexDirection: "row",
  },
  content: {
    flex: 1,
  },
  rightContainer: {
    maxWidth: 40,
    height: 40,
  },
});

export default Item;
