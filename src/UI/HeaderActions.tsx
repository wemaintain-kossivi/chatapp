import * as React from "react";
import { StyleSheet, useWindowDimensions } from "react-native";
import { Text, FAB, Icon, useTheme } from "react-native-elements";
import { User } from "sendbird";

import { Container, IHeaderIcon, IoniconType } from ".";

interface IProps {
  icons: IHeaderIcon[];
}

const Component: React.FC<IProps> = (props) => {
  const { useState } = React;
  const { icons } = props;
  const { theme } = useTheme();

  return (
    <Container noPadding style={styles.container}>
      {(icons || []).map(({ name, onPress, color, disabled }, index) => (
        <Icon
          key={name + index}
          name={name}
          onPress={onPress as any}
          color={color || theme.colors?.grey1}
          containerStyle={{ padding: theme.Spacing?.medium }}
          iconStyle={{ fontSize: theme.fontSizes?.xLarge }}
        />
      ))}
    </Container>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    justifyContent: "flex-start",
  },
});

export default Component;
