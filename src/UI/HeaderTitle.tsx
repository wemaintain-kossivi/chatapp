import * as React from "react";
import { StyleSheet, Pressable } from "react-native";
import { Text, useTheme } from "react-native-elements";

import { Container } from ".";

interface IProps {
  title: string;
  subTitle?: string;
  onPress?: Function;
  color?: string;
}

const Component: React.FC<IProps> = (props) => {
  const { useState } = React;
  const { theme } = useTheme();
  const { title, subTitle, onPress, color } = props;

  return (
    <Pressable onPress={onPress as any} style={styles.container}>
      <Text h4 h4Style={{ color: color || theme.colors?.grey1, fontSize: 20 }}>
        {title}
      </Text>
      <Text style={{ color: color || theme.colors?.grey1 }}>{subTitle}</Text>
    </Pressable>
  );
};

const styles = StyleSheet.create({
  container: {},
});

export default Component;
