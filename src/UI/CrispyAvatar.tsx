import * as React from "react";
import { StyleSheet, ViewStyle } from "react-native";
import { useTheme, Avatar, ListItem } from "react-native-elements";

import { Container, IoniconType } from "./index";

interface Iprops {
  avatarUrl?: string;
  icon?: IoniconType;
  bgColor?: string;
  iconColor?: string;
  onPress?: Function;
  size: any;
  containerStyle?: ViewStyle;
}
const Item: React.FC<Iprops> = (props) => {
  const { useState } = React;
  const { avatarUrl, icon, bgColor, iconColor, onPress, size, containerStyle } =
    props;

  const { theme } = useTheme();

  return !!avatarUrl ? (
    <Avatar
      rounded
      source={{
        uri: avatarUrl,
      }}
      size={size}
      onPress={onPress as any}
      containerStyle={containerStyle}
    ></Avatar>
  ) : (
    <Avatar
      rounded
      icon={{
        name: icon || "person",
        type: "ionicon",
        color: iconColor || theme.colors?.white,
      }}
      onPress={onPress as any}
      size={size || "small"}
      containerStyle={{ backgroundColor: bgColor || theme.colors?.grey2 }}
    />
  );
};
const styles = StyleSheet.create({});

export default Item;
