import { Ionicons } from "@expo/vector-icons";
import * as React from "react";
import { StyleSheet, FlatList, TextStyle } from "react-native";
import {
  Text,
  FAB,
  Icon,
  useTheme,
  SearchBar,
  CheckBox,
} from "react-native-elements";

import {
  Container,
  ICrispyListItem,
  IoniconType,
  ListItem,
  EmptyComponent,
} from "./index";

interface IProps {
  data: ICrispyListItem<any>[];
  onPressItem?: Function;
  inSelectMode?: boolean;
  onValidation?: Function;
  validationIcon?: IoniconType;
  onSearch?: Function;
  onSearchChangeText?: Function;
  searchValue?: string;
  showSearchBar?: boolean;
  searchCancelButtonTitle?: string;
  showChevron?: boolean;
  defaultAvatarIcon?: IoniconType;
  emptyTitle?: string;
  emptyText?: string;
  emptyIcon?: IoniconType;
  onRefresh?: Function;
  refreshing?: boolean;
  itemSubTitleStyle?: TextStyle;
  onEndReached?: Function;
}

const Component: React.FC<IProps> = (props) => {
  const { useState } = React;
  const {
    data,
    onPressItem,
    inSelectMode,
    onValidation,
    onSearch,
    onSearchChangeText,
    showSearchBar,
    validationIcon,
    searchCancelButtonTitle,
    showChevron,
    defaultAvatarIcon,
    emptyTitle,
    emptyText,
    emptyIcon,
    onRefresh,
    refreshing,
    itemSubTitleStyle,
    searchValue,
    onEndReached,
  } = props;
  const { theme } = useTheme();

  const renderItem = ({ item }: { item: ICrispyListItem }) => {
    const onPress = () => {
      if (typeof onPressItem == "function") {
        onPressItem(item);
      }
    };
    return (
      <ListItem
        avatar={item.avatar}
        avatarIcon={defaultAvatarIcon}
        title={item.title}
        subTitle={item.subTitle}
        hasBadge={item.hasBadge}
        onPress={onPress}
        showChevron={showChevron}
        rightComponent={
          inSelectMode ? (
            <Icon
              name="square-outline"
              color={theme.colors?.grey1}
              containerStyle={{
                padding: theme.Spacing?.small,
                width: 60,
                margin: 0,
              }}
              iconStyle={{ fontSize: theme.fontSizes?.xLarge }}
            />
          ) : undefined
        }
        subTitleStyle={itemSubTitleStyle}
      />
    );
  };

  return (
    <Container noPadding style={styles.container} flex>
      {!!showSearchBar && (
        <SearchBar
          placeholder="Type Here..."
          platform="ios"
          onChangeText={onSearchChangeText as any}
          onEndEditing={onSearch as any}
          value={searchValue}
        />
      )}

      <FlatList
        data={data}
        renderItem={renderItem}
        keyExtractor={(item: ICrispyListItem) => item.itemId}
        style={{
          width: "100%",
        }}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{
          paddingBottom: data?.length ? 50 : 0,
          backgroundColor: theme.colors?.white,
          flexGrow: 1,
          justifyContent: data?.length ? "flex-start" : "center",
        }}
        ListEmptyComponent={
          <EmptyComponent
            icon={emptyIcon}
            title={emptyTitle}
            text={emptyText}
          />
        }
        onRefresh={onRefresh as any}
        refreshing={refreshing}
        onEndReached={onEndReached as any}
      />
      {!!validationIcon && (
        <FAB
          placement="right"
          icon={{
            type: "ionicon",
            name: validationIcon,
            color: `${theme.colors?.white}`,
          }}
          onPress={onValidation as any}
        />
      )}
    </Container>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: "column",
    justifyContent: "flex-start",
    backgroundColor: "#ffffff",
  },
});

export default Component;
