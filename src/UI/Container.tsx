import * as React from "react";
import { StyleSheet, View, ViewStyle } from "react-native";
import { useTheme } from "react-native-elements";

interface IProps {
  noPadding?: Boolean;
  style?: ViewStyle;
  flex?: boolean;
}
const Container: React.FC<IProps> = (props) => {
  const { noPadding, children, style, flex } = props;
  const { theme } = useTheme();
  const padding = noPadding ? 0 : theme.Spacing?.small;
  const flexProp = flex ? { flex: 1 } : {};

  return (
    <View
      style={{ ...flexProp, ...styles.container, padding: padding, ...style }}
    >
      {children}
    </View>
  );
};

const styles = StyleSheet.create({
  container: { width: "100%", alignItems: "center", justifyContent: "center" },
});

export default Container;
