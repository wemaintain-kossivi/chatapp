import * as React from "react";
import { Pressable, StyleSheet, useWindowDimensions } from "react-native";
import { useTheme } from "react-native-elements";
import Animated, {
  useSharedValue,
  useAnimatedStyle,
  withSpring,
  interpolate,
  Extrapolate,
} from "react-native-reanimated";

import Container from "./Container";

const BACKDROP_WIDTH = 100;
const PANEL_MAX_WIDTH = 300;

interface IProps {
  panelComponent: React.ReactNode;
  mainComponent: React.ReactNode;
  panelIsVisible?: boolean;
  onPressBackDrop?: Function;
}

const Component: React.FC<IProps> = (props) => {
  const { useEffect } = React;
  const { panelComponent, mainComponent, panelIsVisible, onPressBackDrop } =
    props;
  const { theme } = useTheme();
  const animState = useSharedValue(0);

  const { width: deviceWhitdh } = useWindowDimensions();
  const PANEL_WIDTH = Math.min(PANEL_MAX_WIDTH, deviceWhitdh - BACKDROP_WIDTH);

  useEffect(() => {
    animState.value = withSpring(panelIsVisible ? 1 : 0);
  });

  const containerAnimStyle = useAnimatedStyle(() => {
    const translateX = interpolate(
      animState.value,
      [0, 1],
      [0, -PANEL_WIDTH],
      Extrapolate.CLAMP
    );

    return {
      transform: [{ translateX }],
    };
  });

  return (
    <Animated.View style={[styles.container, containerAnimStyle]}>
      <Container
        noPadding
        flex
        style={{
          ...styles.innerContainer,
        }}
      >
        <Container
          noPadding
          style={{
            width: deviceWhitdh,
            ...styles.main,
          }}
        >
          {mainComponent}
          {panelIsVisible && (
            <Pressable
              style={{ width: deviceWhitdh, ...styles.mask }}
              onPress={onPressBackDrop as any}
            />
          )}
        </Container>

        <Container
          noPadding
          style={{
            width: PANEL_WIDTH,
            ...styles.panel,
          }}
        >
          {panelComponent}
        </Container>
      </Container>
    </Animated.View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "flex-start",
    flexDirection: "row",
    height: "100%",
  },
  innerContainer: {
    flex: 1,
    justifyContent: "flex-start",
    flexDirection: "row",
    height: "100%",
  },
  panel: {
    height: "100%",
  },
  main: {
    flexDirection: "row",
    height: "100%",
    justifyContent: "flex-start",
    overflow: "hidden",
  },
  mask: {
    backgroundColor: "#22222244",
    position: "absolute",
    height: "100%",
    top: 0,
    zIndex: 1000,
  },
});

export default Component;
