import * as React from "react";
import { useWindowDimensions, StyleSheet, View, Image } from "react-native";
import { Ionicons } from "@expo/vector-icons";
import { useTheme, Theme, Text, Avatar } from "react-native-elements";
import Container from "./Container";
import { StatusBar } from "expo-status-bar";
import { IoniconType } from ".";

interface IProps {
  bgIcon: IoniconType;
  bgColor: string | undefined;
  bgIconColor?: string | undefined;
  titleColor?: string | undefined;
  title?: string;
  height?: number;
}

const Component: React.FC<IProps> = (props) => {
  const { bgIcon, bgColor, title, bgIconColor, height, titleColor } = props;

  const { height: deviceHeight } = useWindowDimensions();
  const { theme } = useTheme();

  const style = {
    height: height || (deviceHeight * 1) / 3,
    width: "100%",
    backgroundColor: bgColor || theme.colors?.primary,
    padding: theme.Spacing?.large,
  };

  return (
    <Container style={{ ...style, ...styles.container }}>
      <View
        style={{
          transform: [
            {
              rotateZ: "-20deg",
            },
          ],
          position: "absolute",
          right: -40,
          bottom: -70,
          opacity: 0.1,
        }}
      >
        <Ionicons
          name={bgIcon}
          size={200}
          color={bgIconColor || theme.colors?.primary}
        />
      </View>

      <Text h2 style={{ color: titleColor || theme.colors?.primary }}>
        {title}
      </Text>
    </Container>
  );
};

const styles = StyleSheet.create({
  container: { alignItems: "flex-end", justifyContent: "flex-end" },
});

export default Component;
