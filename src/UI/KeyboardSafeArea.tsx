import * as React from "react";
import {
  StyleSheet,
  Pressable,
  KeyboardAvoidingView,
  Keyboard,
  ViewStyle,
  ScrollView,
} from "react-native";
import { Text, useTheme } from "react-native-elements";

import { Container } from ".";

interface IProps {
  style?: ViewStyle;
  offset?: number;
  noFlex?: boolean;
}

const Component: React.FC<IProps> = (props) => {
  const { useState } = React;
  const { theme } = useTheme();
  const { children, style, offset, noFlex } = props;
  const flexProp = noFlex ? {} : { flex: 1 };
  return (
    <KeyboardAvoidingView
      style={{ ...flexProp, width: "100%", ...style }}
      behavior="padding"
      keyboardVerticalOffset={offset}
    >
      <Pressable style={{ flex: 1 }} onPress={() => Keyboard.dismiss()}>
        {children}
      </Pressable>
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  container: {},
});

export default Component;
