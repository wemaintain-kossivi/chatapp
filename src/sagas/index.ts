import { spawn } from "redux-saga/effects";

import authSaga from "./authSaga";
import connectivitySaga from "./connectivitySaga";
import ChannelSaga from "./channelSaga";
import userSaga from "./userSaga";
import messageSaga from "./messageSaga";
import socketsSaga from "./socketSaga";

export default function* rootSaga() {
  yield spawn(authSaga);
  yield spawn(connectivitySaga);
  yield spawn(ChannelSaga);
  yield spawn(userSaga);
  yield spawn(messageSaga);
  yield spawn(socketsSaga);
}
