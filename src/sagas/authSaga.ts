import { call, fork, put, take } from "redux-saga/effects";

import { User } from "sendbird";

import { ERROR_CODE, IAction, IError } from "../types";
import * as Api from "../Api";
import ACTION_TYPES from "../store/actionTypes";
import { loginSuccess, loginError } from "../store/actions";
const { LOGIN, LOGOUT, LOGIN_ERROR } = ACTION_TYPES;

function* signInSaga(payload: string) {
  try {
    const user: User = yield call(Api.loginSendBird, payload);
    yield put(loginSuccess(user));
  } catch (e) {
    const error: IError = {
      code: ERROR_CODE.DEFAULT,
      msg: e,
    };
    yield put(loginError(error));
  }
}

function* authFlowSaga() {
  while (true) {
    const { payload } = yield take(LOGIN);
    yield fork(signInSaga, payload);
    yield take([LOGOUT, LOGIN_ERROR]);
    yield call(Api.logOutSendBird);
  }
}

export default authFlowSaga;
