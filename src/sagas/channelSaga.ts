import { call, fork, put, spawn, take, takeLatest } from "redux-saga/effects";
import { Channel, eventChannel } from "redux-saga";
import { OpenChannel, PreviousMessageListQuery } from "sendbird";

import ACTION_TYPES from "../store/actionTypes";
import * as Api from "../Api";
import {
  channelFetchSuccess,
  channelFetchError,
  channelSelectSucsess,
  channelSelectError,
  messageFetch,
  messagePurge,
  channelEditError,
  channelEditSucsess,
} from "../store/actions";
import {
  ERROR_CODE,
  IActiveChannel,
  IChannelAction,
  IError,
  IMapChannel,
} from "../types";
const { CHANNEL_FETCH, CHANNEL_SELECT, JOIN_CHANNEL, CHANNEL_EDIT } =
  ACTION_TYPES;

function* fetchCall(searchText: string) {
  try {
    const channels: IMapChannel = yield call(Api.fetchChannel, searchText);
    yield put(channelFetchSuccess(channels));
  } catch (e) {
    const error: IError = {
      code: ERROR_CODE.CHANNEL_FETCH,
      msg: e,
    };
    yield put(channelFetchError(error));
  }
}

function* fetchSaga() {
  while (true) {
    const { payload } = yield take(CHANNEL_FETCH);
    yield call(fetchCall, payload);
  }
}

function* selectChannelCall(payload: OpenChannel) {
  try {
    const listQuery: PreviousMessageListQuery = yield call(
      Api.createMessageListQuery,
      payload
    );

    const successPayload: IActiveChannel = {
      channel: payload,
      listQuery: listQuery,
    };
    yield put(messagePurge());
    yield put(channelSelectSucsess(successPayload));
    yield put(messageFetch());
  } catch (e) {
    const error: IError = {
      code: ERROR_CODE.CHANNEL_FETCH,
      msg: e,
    };
    yield put(channelSelectError(error));
  }
}

function* selectSaga() {
  while (true) {
    const { payload } = yield take(CHANNEL_SELECT);
    yield call(selectChannelCall, payload);
  }
}

export function* joinChannelFlow(payload: OpenChannel) {
  try {
    const joined: boolean = yield call(Api.joinChannel, payload);
    return joined;
  } catch (e) {
    console.log("saga: join channel failed error:  " + e);
    return;
  }
}

function* joinSaga() {
  while (true) {
    const { payload } = yield take(JOIN_CHANNEL);
    yield call(joinChannelFlow, payload);
  }
}

function* channelEditFlow(payload: IChannelAction) {
  try {
    const channel: OpenChannel = yield call(Api.editChannel, payload);
    yield put(channelEditSucsess(channel));
  } catch (e) {
    const error: IError = {
      code: ERROR_CODE.CHANNEL_EDIT_ERROR,
      msg: e,
    };
    yield put(channelEditError(error));
  }
}

function* chanelEdit() {
  while (true) {
    const { payload } = yield take(CHANNEL_EDIT);
    yield call(channelEditFlow, payload);
  }
}

function* channelSaga() {
  yield spawn(fetchSaga);
  yield spawn(selectSaga);
  yield spawn(joinSaga);
  yield spawn(chanelEdit);
}

export default channelSaga;
