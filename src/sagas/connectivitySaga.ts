import { call, fork, put, take } from "redux-saga/effects";
import { Channel, eventChannel } from "redux-saga";
import * as NetInfo from "@react-native-community/netinfo";
import { refreshOnlineStatus } from "../store/actions";

function* connectivityChannel() {
  const chan = eventChannel((handler) => {
    const unSuscribe = NetInfo.addEventListener((state) => {
      handler(state.isConnected);
    });
    return unSuscribe;
  });

  while (true) {
    const internetInfo: boolean = yield take(chan);
    yield put(refreshOnlineStatus(internetInfo));
  }
}

export default function* connectivitySaga() {
  yield call(connectivityChannel);
}
