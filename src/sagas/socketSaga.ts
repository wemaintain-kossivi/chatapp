import {
  call,
  fork,
  put,
  take,
  takeLatest,
  spawn,
  select,
} from "redux-saga/effects";
import { Channel, eventChannel } from "redux-saga";
import { OpenChannel, UserMessage, UserMessageParams } from "sendbird";
import ACTION_TYPES from "../store/actionTypes";
import * as Api from "../Api";
import { newUserNotif, messageFetch } from "../store/actions";
import {
  ERROR_CODE,
  IActiveChannel,
  IError,
  IMap,
  ISendPayload,
} from "../types";
import { selectedChannel } from "../store/selectors";
import { joinChannelFlow } from "./channelSaga";

import {
  SB,
  channelEventHandler,
  UNIQUE_HANDLER_ID,
  removeHandlerLister,
} from "../Api";

const { MESSAGE_FETCH, MESSAGE_SEND, JOIN_CHANNEL } = ACTION_TYPES;

function* channelSocketHandler() {
  const chan = eventChannel((handler) => {
    channelEventHandler.onUserEntered = (channel, user) => {
      console.log("user has entered");
      handler(channel);
    };
    SB.addChannelHandler(UNIQUE_HANDLER_ID, channelEventHandler);
    return removeHandlerLister;
  });

  while (true) {
    yield take(chan);
    //console.log("do add notif");
    yield put(newUserNotif());
  }
}

function* socketSaga() {
  yield spawn(channelSocketHandler);
}

export default socketSaga;
