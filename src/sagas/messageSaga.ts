import { call, put, take, spawn, select } from "redux-saga/effects";
import { eventChannel } from "redux-saga";
import { UserMessage } from "sendbird";

import ACTION_TYPES from "../store/actionTypes";
import * as Api from "../Api";
import {
  messageFetchSuccess,
  messageFetchError,
  messageSendError,
  messageSendSuccess,
  messageFetch,
  messagePurge,
  addMessageRecieved,
} from "../store/actions";
import {
  ERROR_CODE,
  IActiveChannel,
  IError,
  IMap,
  ISendPayload,
} from "../types";
import { selectedChannel } from "../store/selectors";
import { joinChannelFlow } from "./channelSaga";

import {
  SB,
  channelEventHandler,
  UNIQUE_HANDLER_ID,
  removeHandlerLister,
} from "../Api";

const { MESSAGE_FETCH, MESSAGE_SEND, JOIN_CHANNEL } = ACTION_TYPES;

function* fetchSaga() {
  while (true) {
    try {
      yield take(MESSAGE_FETCH);
      const activeChannel: IActiveChannel = yield select(selectedChannel);
      const messages: IMap<UserMessage> = yield call(
        Api.fetchMessages,
        activeChannel
      );
      yield put(messageFetchSuccess(messages));
    } catch (e) {
      const error: IError = {
        code: ERROR_CODE.MESSAGE_FETCH,
        msg: e,
      };
      yield put(messageFetchError(error));
    }
  }
}

function* sendSaga(payload: ISendPayload) {
  try {
    const message: UserMessage = yield call(Api.sendMessage, payload);
    yield put(messageSendSuccess(message));
    yield put(messageFetch());
  } catch (e) {
    const error: IError = {
      code: ERROR_CODE.MESSAGE_SEND,
      msg: e,
    };
    yield put(messageSendError(error));
  }
}

function* sendFlowSaga() {
  while (true) {
    const { payload } = yield take(MESSAGE_SEND);
    const { channel } = payload;
    try {
      const joined: boolean = yield call(joinChannelFlow, channel);
      if (!joined) {
        return;
      }
      yield call(sendSaga, payload);
    } catch (e) {
      console.log("sendFlowSaga do call failed error :" + e);
    }
  }
}

function* sbMessageHandler() {
  const recievedChannel = eventChannel((handler) => {
    channelEventHandler.onMessageReceived = (channel, message) => {
      handler(message);
    };

    SB.addChannelHandler(UNIQUE_HANDLER_ID, channelEventHandler);
    return removeHandlerLister;
  });
  const updatedChannel = eventChannel((handler) => {
    channelEventHandler.onMessageUpdated = (channel, message) => {
      handler(message);
    };

    SB.addChannelHandler(UNIQUE_HANDLER_ID, channelEventHandler);
    return removeHandlerLister;
  });

  function* recieveHandler() {
    const message: UserMessage = yield take(recievedChannel);
    const activeChannel: IActiveChannel = yield select(selectedChannel);
    if (message.channelUrl == activeChannel.channel.url) {
      yield put(addMessageRecieved(message));
    }
    return;
  }

  function* updatedHandler() {
    yield take(recievedChannel);
    yield put(messageFetch());
  }

  while (true) {
    yield spawn(recieveHandler);
    yield spawn(updatedHandler);
  }
}

function* messageSaga() {
  yield spawn(fetchSaga);
  yield spawn(sendFlowSaga);
  yield spawn(sbMessageHandler);
}

export default messageSaga;
