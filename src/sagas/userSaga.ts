import { call, fork, put, take, takeLatest } from "redux-saga/effects";
import { Channel, eventChannel } from "redux-saga";
import { User } from "sendbird";

import ACTION_TYPES from "../store/actionTypes";
import * as Api from "../Api";
import { usersFetchSuccess, userFetchError } from "../store/actions";
import { ERROR_CODE, IError, IMap } from "../types";
const { USER_FETCH } = ACTION_TYPES;

function* fetchSaga() {
  try {
    const users: IMap<User> = yield call(Api.fetchUsers);
    yield put(usersFetchSuccess(users));
  } catch (e) {
    console.log("saga users fail e :" + e);
    const error: IError = {
      code: ERROR_CODE.USER_FETCH,
      msg: e,
    };
    yield put(userFetchError(error));
  }
}
function* fetchUser() {}

function* userSaga() {
  yield takeLatest(USER_FETCH, fetchSaga);
}

export default userSaga;
