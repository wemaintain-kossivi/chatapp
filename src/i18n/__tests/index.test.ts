import { IPhrases, selectPhrases, localeKey } from "../index";

describe("i18n lang seletor", () => {
  const PHRASES_EN = {
    qwerty: "qwerty",
  };
  const PHRASES_FR = {
    qwerty: "azerty",
  };
  const PHRASES: IPhrases = {
    en: PHRASES_EN,
    fr: PHRASES_FR,
  };
  it("lang FR", () => {
    const expectation = PHRASES_FR;
    expect(selectPhrases("fr", PHRASES)).toEqual(PHRASES_FR);
  });

  it("lang EN", () => {
    const expectation = PHRASES_FR;
    expect(selectPhrases("en", PHRASES)).toEqual(PHRASES_EN);
  });

  it("lang exotic", () => {
    const expectation = PHRASES_FR;
    expect(selectPhrases("zoulou", PHRASES)).toEqual(PHRASES_EN);
  });

  it("lang undefined", () => {
    const expectation = PHRASES_FR;
    expect(selectPhrases(undefined, PHRASES)).toEqual(PHRASES_EN);
  });
});

describe("i18n locale key", () => {
  const en_EN = "en_EN";
  const en = "en";
  const e = "e";

  it("localeKey en_EN", () => {
    const expectation = "en";
    expect(localeKey(en_EN)).toEqual("en");
  });
  it("localeKey en", () => {
    const expectation = "en";
    expect(localeKey(en)).toEqual("en");
  });
  it("localeKey e", () => {
    const expectation = "e";
    expect(localeKey(e)).toEqual("e");
  });
});
