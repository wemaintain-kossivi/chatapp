import * as Localization from "expo-localization";

export interface IPhrases {
  [key: string]: {};
}
const LANG_KEYS = {
  EN: "en",
  FR: "fr",
};

const localeRaw = Localization.locale;
export const localeKey = (raw: string): string => {
  return raw.length > 2 ? raw.substring(0, 2) : raw;
};
export const locale = localeKey(localeRaw);

const PHRASES: IPhrases = {
  [LANG_KEYS.EN]: require("./phrases/en.json"),
  [LANG_KEYS.FR]: require("./phrases/fr.json"),
};

export const selectPhrases = (
  locale: string | undefined,
  phrases: IPhrases
) => {
  const dico = !!locale ? phrases[locale] : undefined;
  return dico !== undefined ? dico : phrases[LANG_KEYS.EN];
};

export const phrases = selectPhrases(locale, PHRASES);
