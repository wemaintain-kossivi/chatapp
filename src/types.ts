import {
  OpenChannel,
  PreviousMessageListQuery,
  User,
  UserMessage,
  UserMessageParams,
} from "sendbird";
import { Map } from "immutable";

import { ICrispyListItem } from "./UI";

// Store
export interface IAction<T = undefined> {
  type: string;
  payload?: T;
}
/*export interface IMap<T> {
  data: {
    [key: string]: T;
  };
  indexes: string[];
}
*/
export interface IMap<T> extends Map<string, T> {}
export enum ERROR_CODE {
  DEFAULT,
  CHANNEL_FETCH,
  CHANNEL_SELECT,
  USER_FETCH,
  MESSAGE_FETCH,
  MESSAGE_SEND,
  CHANNEL_EDIT_ERROR,
}
export interface IError {
  code: ERROR_CODE;
  msg?: string;
}
export const MAP_FIELDS_PATH = {
  data: "data",
  indexes: "indexes",
};

export interface IApp {
  isOnline?: boolean;
  notifs?: IMapNotif;
  hasNewNotif?: boolean;
}
export interface IAuth {
  user?: User;
  error?: IError;
  isFetching?: boolean;
}

export interface IMapChannel extends Map<string, OpenChannel> {}
export interface IChannel {
  isFetching?: boolean;
  isSelecting?: boolean;
  isEditing?: boolean;
  activeChannel?: IActiveChannel;
  map?: IMapChannel;
  error?: IError;
}

export interface IMapUser extends Map<string, User> {}
export interface IUser {
  isFetching?: boolean;
  map: IMapUser;
  error?: IError;
}

export interface IMessage {
  isFetching?: boolean;
  isSending?: boolean;
  map: IMap<UserMessage>;
  error?: IError;
  sendError?: IError;
}
export interface IMapMessage extends Map<string, UserMessage> {}

export interface ISendPayload {
  channel: OpenChannel;
  content: string;
}

export interface IActiveChannel<T = PreviousMessageListQuery> {
  channel: OpenChannel;
  listQuery: T;
}

export interface IChannelAction {
  chanelName: string;
  coverUrl?: string;
  channelUrl?: string;
}

export enum NOTIF_TYPE {
  DEFAULT,
  LOGGED_ON,
}

export interface INotificationBase {
  title?: string;
  desc?: string;
  type: NOTIF_TYPE;
}
export interface INotification<T = undefined> extends INotificationBase {
  id: string;
  markAsRead?: boolean;
  raw?: T;
  createdAt?: string;
}
export interface IMapNotif extends Map<string, INotification> {}

// components interfaces
export interface IMessageUI {
  itemId: string;
  sender?: string;
  content?: string;
  avatar?: string;
  raw?: UserMessage;
}
