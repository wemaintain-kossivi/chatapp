import * as React from "react";
import renderer from "react-test-renderer";
import { I18n } from "react-polyglot";

import SignInScreen from "../Login/SignIn";

describe(" login flow", () => {
  const doLogin = () => {};
  const abort = () => {};
  const phrases = {
    "Login title": "Connexion",
    "Login input pseudo": "Pseudo",
    "Login btn label": "Connexion",
    "Login login abort": "Annuler",
  };

  it("loginSign  default  state", () => {
    const signInJson = renderer

      .create(
        <I18n locale="en" messages={phrases}>
          <SignInScreen
            doLogin={doLogin}
            abortLogin={abort}
            isLoading={false}
          />
        </I18n>
      )
      .toJSON();
    expect(signInJson).toMatchSnapshot();
  });

  it("loginSign  in loading  state", () => {
    const signInJson = renderer
      .create(
        <I18n locale="en" messages={phrases}>
          <SignInScreen doLogin={doLogin} abortLogin={abort} isLoading={true} />
        </I18n>
      )
      .toJSON();
    expect(signInJson).toMatchSnapshot();
  });
});
