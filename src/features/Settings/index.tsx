import * as React from "react";
import { Alert } from "react-native";
import { Text, FAB, Avatar, useTheme, Header } from "react-native-elements";
import { useDispatch, useSelector } from "react-redux";
import { useTranslate } from "react-polyglot";
import { StackScreenProps } from "@react-navigation/stack";
import Constants from "expo-constants";

import { SettingsStackParamList } from "../../navigation/types";
import { logOut, purgeApp } from "../../store/actions";
import { authUserAvatarUrl } from "../../store/selectors";
import { Container, CrispyAvatar, BigHeader, HeaderTitle } from "../../UI";
import SettingRow from "./SettingRow";

interface IProps {
  navigation: StackScreenProps<SettingsStackParamList>;
}
const ProfilComponent: React.FC<IProps> = (props) => {
  const dispatch = useDispatch();
  const t = useTranslate();
  const { theme } = useTheme();

  const doLogOut = () => {
    dispatch(logOut());
  };
  const doPurge = () => {
    dispatch(purgeApp());
  };

  const userAvatarUrl = useSelector(authUserAvatarUrl);
  return (
    <Container flex noPadding>
      <Header
        centerComponent={
          <HeaderTitle
            title={t("Menu Settings")}
            subTitle=""
            color={theme.colors?.white}
          />
        }
        placement="left"
        backgroundColor={theme.colors?.primary}
        containerStyle={{ borderBottomWidth: 0 }}
      />
      <Container noPadding>
        <BigHeader
          bgColor={theme.colors?.primary}
          bgIcon="settings-outline"
          title={""}
          height={120}
          bgIconColor={theme.colors?.white}
          titleColor={theme.colors?.white}
        />
      </Container>

      <Container
        flex
        style={{
          backgroundColor: theme.colors?.white,
          justifyContent: "flex-start",
        }}
        noPadding
      >
        {/** profil */}
        <Container style={{ paddingTop: theme.Spacing?.xLarge }}>
          <SettingRow
            icon="person-outline"
            title={t("My profil")}
            subTitle="-- Work in progress"
          />
          <Container
            style={{
              position: "absolute",
              top: -50,
            }}
          >
            <CrispyAvatar
              size="large"
              bgColor={theme.colors?.white}
              iconColor={theme.colors?.primary}
              avatarUrl={userAvatarUrl}
              containerStyle={{
                backgroundColor: theme.colors?.primary,
                padding: 5,
              }}
            />
          </Container>
        </Container>

        {/** other settings */}
        <Container style={{ backgroundColor: theme.colors?.grey5 }} />
        <SettingRow
          icon="log-out-outline"
          title={t("Logout")}
          subTitle=""
          onPress={doLogOut}
        />
        <SettingRow
          icon="pricetag-outline"
          title={t("About Version")}
          subTitle={Constants.manifest.version}
        />

        <SettingRow
          icon="sync-outline"
          title={t("Purge")}
          subTitle={t("Purge explanation")}
          onPress={doLogOut}
          isDanger
        />
      </Container>
    </Container>
  );
};

export default ProfilComponent;
