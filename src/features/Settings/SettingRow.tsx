import * as React from "react";
import { Alert } from "react-native";
import { useTheme, Icon, ListItem } from "react-native-elements";
import { useTranslate } from "react-polyglot";

import { Ionicons } from "@expo/vector-icons";

interface Iprops {
  icon: keyof typeof Ionicons.glyphMap;
  title?: string;
  subTitle?: string;
  onPress?: Function;
  showChevron?: boolean;
  isDanger?: boolean;
}
const Screen: React.FC<Iprops> = (props) => {
  const { icon, title, subTitle, onPress, showChevron, isDanger } = props;
  const { theme } = useTheme();
  const t = useTranslate();

  const dangerStyle = isDanger ? { color: theme.colors?.error } : {};
  const confirmAction = () => {
    Alert.alert(t("Calm down"), t("Are u sure bro ?"), [
      {
        text: t("Cancel"),

        style: "cancel",
      },
      { text: t("OK"), onPress: onPress as any },
    ]);
  };
  return (
    <ListItem
      bottomDivider
      onPress={isDanger ? confirmAction : (onPress as any)}
      style={{ width: "100%" }}
      containerStyle={{ padding: theme.Spacing?.small }}
    >
      <Icon name={icon} raised type="ionicon" />
      <ListItem.Content>
        <ListItem.Title style={dangerStyle}>{title}</ListItem.Title>
        <ListItem.Subtitle numberOfLines={1}>{subTitle}</ListItem.Subtitle>
      </ListItem.Content>
      {showChevron && <ListItem.Chevron />}
    </ListItem>
  );
};

export default Screen;
