import * as React from "react";
import { View, StyleSheet, useWindowDimensions } from "react-native";
import { Input, useTheme, FAB, Button, Icon } from "react-native-elements";
import { useTranslate } from "react-polyglot";
import { useDispatch } from "react-redux";
import { KeyboardAvoidingView, ScrollView } from "react-native";
import Ionicons from "@expo/vector-icons";

import { Container } from "../../UI";

interface IProps {
  isLoading: boolean;
  doLogin: Function;
  abortLogin: Function;
}
const Component: React.FC<IProps> = (props) => {
  const { useState } = React;
  const { doLogin, isLoading, abortLogin } = props;
  const [pseudo, setPseudo] = useState("");
  const { theme } = useTheme();
  const t = useTranslate();
  const { width: screenWidth } = useWindowDimensions();

  return (
    <Container
      flex
      style={{
        backgroundColor: `${theme.colors?.primary}`,
        overflow: "hidden",
      }}
      noPadding
    >
      <View
        style={{
          ...styles.bgDesign,
          width: screenWidth,
          height: screenWidth,
          top: -screenWidth / 2,
          transform: [{ rotateZ: "45deg" }],
        }}
      />
      <View
        style={{
          ...styles.bgDesign,
          width: screenWidth,
          height: screenWidth,
          top: (-screenWidth * 3) / 4,
          transform: [{ rotateZ: "70deg" }],
        }}
      />
      <Input
        placeholder={t("Login input pseudo")}
        leftIcon={{
          type: "ionicon",
          name: "person",
          color: `${theme.colors?.grey3}`,
        }}
        onChangeText={setPseudo}
        value={pseudo}
        placeholderTextColor={theme.colors?.grey4}
        autoCorrect={false}
      />

      <Icon
        reverse
        name="log-in-outline"
        type="ionicon"
        color={theme.colors?.secondary}
        reverseColor={theme.colors?.white}
        onPress={() => doLogin(pseudo)}
        disabled={!pseudo || isLoading}
      />
    </Container>
  );
};

const styles = StyleSheet.create({
  bgDesign: {
    position: "absolute",
    backgroundColor: "#ffffff22",
    borderRadius: 10,
  },
});

export default Component;
