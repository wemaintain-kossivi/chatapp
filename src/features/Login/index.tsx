import { StackScreenProps } from "@react-navigation/stack";
import * as React from "react";
import {
  StyleSheet,
  KeyboardAvoidingView,
  Pressable,
  Keyboard,
} from "react-native";
import { Text, useTheme, LinearProgress } from "react-native-elements";
import { useTranslate } from "react-polyglot";
import { useDispatch, useSelector } from "react-redux";

import { LoginStackParamList } from "../../navigation/types";
import { Container, BigHeader, KeyboardSafeArea } from "../../UI";
import { login, loginAbort } from "../../store/actions";
import { authIsFetching, authError } from "../../store/selectors";

import SigIn from "./SignIn";
import { ScrollView } from "react-native-gesture-handler";

interface IProps {
  navigation: StackScreenProps<LoginStackParamList>;
}
const LoginContainer: React.FC<IProps> = (props) => {
  const { useState } = React;
  const { navigation } = props;
  const { theme } = useTheme();
  const t = useTranslate();
  const dispatch = useDispatch();

  const isLoading = useSelector(authIsFetching);
  const loginError = useSelector(authError);

  const _doLogin = (pseudo: string) => {
    dispatch(login(pseudo));
  };

  const _abortLogin = () => {
    dispatch(loginAbort());
  };
  return (
    <KeyboardSafeArea>
      <Container noPadding flex>
        <BigHeader
          bgColor={theme.colors?.white}
          bgIcon="qr-code-outline"
          title={t("Login title")}
        />
        {isLoading && <LinearProgress color="secondary" />}
        <Container flex noPadding>
          <SigIn
            doLogin={_doLogin}
            abortLogin={_abortLogin}
            isLoading={isLoading}
          />
        </Container>
        {!!loginError && <Text>{`${loginError.msg}`}</Text>}
      </Container>
    </KeyboardSafeArea>
  );
};

const styles = StyleSheet.create({});

export default LoginContainer;
