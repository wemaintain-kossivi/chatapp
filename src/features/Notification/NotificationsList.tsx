import { StackScreenProps } from "@react-navigation/stack";
import * as React from "react";
import { StyleSheet, TouchableOpacity, View } from "react-native";
import { Text, Header, useTheme } from "react-native-elements";
import { useTranslate } from "react-polyglot";
import { useDispatch, useSelector } from "react-redux";

import { RootStackParamList } from "../../navigation/types";
import { EmptyComponent, Container, HeaderTitle } from "../../UI";
import { markALLNotifsAsRead } from "../../store/actions";
import { appNotifs, appHasNotification } from "../../store/selectors";

interface IProps {}
const OfflineScreen = () => {
  const { useEffect } = React;
  const t = useTranslate();
  const { theme } = useTheme();
  const dispatch = useDispatch();
  const notifications = useSelector(appNotifs);
  const hasNotif = useSelector(appHasNotification);

  useEffect(() => {
    if (hasNotif) {
      dispatch(markALLNotifsAsRead());
    }
  }, [hasNotif, notifications]);

  return (
    <Container flex noPadding>
      <Header
        centerComponent={
          <HeaderTitle
            title={t("Menu Notifications")}
            subTitle=""
            color={theme.colors?.white}
          />
        }
        placement="left"
        backgroundColor={theme.colors?.primary}
      />
      <EmptyComponent
        icon="notifications-outline"
        title={t("Notifications empty")}
      />
    </Container>
  );
};

export default OfflineScreen;
