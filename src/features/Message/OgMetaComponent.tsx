import * as React from "react";
import { TextStyle, ViewStyle } from "react-native";
import { Text, Image, useTheme } from "react-native-elements";
import * as Linking from "expo-linking";

import { OGMetaData } from "sendbird";
import { Container } from "../../UI";

interface Iprops {
  metaData: OGMetaData;
}
const Component: React.FC<Iprops> = (props) => {
  const { metaData } = props;
  const { theme } = useTheme();
  const titleStyle: TextStyle = {
    fontSize: theme.fontSizes?.medium,
    marginBottom: theme.Spacing?.medium,
  };
  const linkStyle: TextStyle = {
    marginTop: theme.Spacing?.medium,
    alignSelf: "flex-end",
    color: theme.colors?.secondary,
  };
  return (
    <Container
      style={{
        justifyContent: "flex-start",
        alignItems: "flex-start",
      }}
    >
      <Image
        source={{ uri: metaData.defaultImage.url }}
        style={{ height: 100, width: 100 }}
      />
      <Text h4 h4Style={titleStyle}>
        {metaData.title}
      </Text>
      <Text>{metaData.description}</Text>
      <Text
        style={linkStyle}
        onPress={() => {
          Linking.openURL(metaData.url);
        }}
      >
        {metaData.url}
      </Text>
    </Container>
  );
};

export default Component;
