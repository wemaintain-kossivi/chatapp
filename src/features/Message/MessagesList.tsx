import * as React from "react";
import { StyleSheet, FlatList } from "react-native";
import {
  Text,
  FAB,
  Icon,
  useTheme,
  SearchBar,
  LinearProgress,
} from "react-native-elements";
import { useSelector, useDispatch } from "react-redux";

import { Container, ListItem } from "../../UI";
import MessageListItem from "./index";
import { IMessageUI } from "../../types";
import {
  messageIsFetching,
  messagesList,
  selectedChannel,
} from "../../store/selectors";
import { messageFetch } from "../../store/actions";

export interface ICrispyListItem {
  itemId: string;
  title?: string;
  subTitle: string;
  avatar?: React.ReactNode;
}

interface IProps {}

const Component: React.FC<IProps> = (props) => {
  const { useState, useRef, useEffect } = React;
  const { theme } = useTheme();
  const dispatch = useDispatch();

  const channel = useSelector(selectedChannel);
  const isFetching = useSelector(messageIsFetching);
  const data = useSelector(messagesList);

  const refreshList = () => {
    if (channel) dispatch(messageFetch());
  };

  const renderItem = ({ item }: { item: IMessageUI }) => {
    return <MessageListItem {...item} avatar={item.raw?.sender.profileUrl} />;
  };

  return (
    <Container noPadding style={styles.container} flex>
      <FlatList
        inverted
        data={data}
        renderItem={renderItem}
        keyExtractor={(item: IMessageUI) => item.itemId}
        style={{ width: "100%" }}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
        onEndReached={refreshList}
        refreshing={isFetching}
        contentContainerStyle={{ paddingBottom: theme.Spacing?.medium }}
      />
      <Container noPadding style={{ position: "absolute", top: 0 }}>
        {isFetching && <LinearProgress color="secondary" />}
      </Container>
    </Container>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: "column",
    justifyContent: "flex-start",
  },
});

export default Component;
