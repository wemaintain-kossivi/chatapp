import * as React from "react";
import { StyleSheet, ViewStyle } from "react-native";
import { useTheme, ListItem, Text } from "react-native-elements";

import { lastSeenLabel } from "../../helpers";
import { IMessageUI } from "../../types";
import { Container, CrispyAvatar } from "../../UI";
import OGMetaComponent from "./OgMetaComponent";

interface Iprops extends IMessageUI {}
const Item: React.FC<Iprops> = (props) => {
  const { useState } = React;
  const { avatar, sender, content, raw } = props;

  const { theme } = useTheme();

  const contentStyle: ViewStyle = {
    backgroundColor: theme.colors?.grey5,
    borderRadius: 10,
    padding: theme.Spacing?.medium,
    maxWidth: "80%",
    alignSelf: "flex-start",
  };

  return (
    <ListItem containerStyle={styles.constainer}>
      <CrispyAvatar avatarUrl={avatar} size="small" />

      <Container noPadding flex style={styles.listRow}>
        <ListItem.Content style={[styles.content, contentStyle]}>
          <ListItem.Title style={{ fontSize: theme.fontSizes?.small }}>
            {sender}
          </ListItem.Title>
          <ListItem.Subtitle style={{ fontSize: theme.fontSizes?.small }}>
            {content}
          </ListItem.Subtitle>
          <Text
            style={{
              fontSize: theme.fontSizes?.xSmall,
              color: theme.colors?.grey3,
            }}
          >
            {lastSeenLabel(raw?.createdAt + "")}
          </Text>
          {raw?.ogMetaData && <OGMetaComponent metaData={raw.ogMetaData} />}
        </ListItem.Content>
      </Container>
    </ListItem>
  );
};
const styles = StyleSheet.create({
  constainer: {
    width: "100%",
    alignItems: "flex-start",
    paddingBottom: 0,
  },
  listRow: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-start",
  },
  content: {
    flex: 1,
  },
  rightContainer: {
    maxWidth: 40,
  },
});

export default Item;
