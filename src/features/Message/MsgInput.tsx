import * as React from "react";
import { StyleSheet, useWindowDimensions, Keyboard } from "react-native";
import {
  Text,
  FAB,
  Divider,
  Button,
  Icon,
  useTheme,
  Input,
} from "react-native-elements";
import { useTranslate } from "react-polyglot";

import { Container, KeyboardSafeArea } from "../../UI";
import { selectedChannel } from "../../store/selectors";
import { messageSend } from "../../store/actions";
import { useDispatch, useSelector } from "react-redux";
import { IActiveChannel, ISendPayload } from "../../types";
import { OpenChannel } from "sendbird";

const KEYBOARD_SAFE_OFFSET = 180;

interface IProps {}

const Component: React.FC<IProps> = (props) => {
  const { useState } = React;
  const [msgContent, setMsgContent] = useState("");
  const [inputHeight, setInputHeight] = useState(70);

  const t = useTranslate();
  const { theme } = useTheme();
  const dispatch = useDispatch();

  const { width: deviceWidth } = useWindowDimensions();
  const activeChannel = useSelector(selectedChannel);
  const { channel } = activeChannel || {};

  const sendMsg = () => {
    if (channel) {
      const payload: ISendPayload = {
        channel: channel,
        content: msgContent,
      };
      dispatch(messageSend(payload));

      setMsgContent("");
    }
  };
  const updateSize = (height: number) => {
    setInputHeight(height);
  };

  return (
    <KeyboardSafeArea
      offset={KEYBOARD_SAFE_OFFSET}
      style={{ height: 70 }}
      noFlex
    >
      <Container
        noPadding
        style={{
          ...styles.container,
          width: deviceWidth,
          backgroundColor: theme.colors?.white,
          height: inputHeight,
        }}
      >
        <Divider />
        <Container flex style={{ ...styles.innerContainer }} noPadding>
          <Input
            placeholder="edit msg"
            containerStyle={{
              ...styles.inputContainer,
              height: inputHeight,
            }}
            inputContainerStyle={{ ...styles.input }}
            style={{ fontSize: theme.fontSizes?.small }}
            onChangeText={setMsgContent}
            value={msgContent}
            multiline
            inputStyle={{ height: inputHeight }}
            onContentSizeChange={(e) =>
              updateSize(e.nativeEvent.contentSize.height)
            }
          />
          <Icon
            name="send"
            onPress={sendMsg}
            color={theme.colors?.grey1}
            containerStyle={{
              padding: theme.Spacing?.small,
              width: 60,
              margin: 0,
              alignSelf: "flex-start",
            }}
            iconStyle={{ fontSize: theme.fontSizes?.xLarge }}
          />
        </Container>
      </Container>
    </KeyboardSafeArea>
  );
};

const styles = StyleSheet.create({
  container: {
    minHeight: 70,
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  innerContainer: {
    flexDirection: "row",
    justifyContent: "center",
  },
  inputContainer: {
    flex: 1,
    height: "100%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    padding: 0,
  },
  input: {
    borderRadius: 5,
    flex: 1,
    margin: 0,
    height: 50,
    backgroundColor: "#ffffff",
    borderColor: "transparent",
  },
});

export default Component;
