import * as React from "react";
import { StyleSheet, useWindowDimensions } from "react-native";
import {
  Text,
  FAB,
  Divider,
  Button,
  Icon,
  useTheme,
} from "react-native-elements";
import { useTranslate } from "react-polyglot";
import { useNavigation } from "@react-navigation/native";
import { StackScreenProps } from "@react-navigation/stack";

import { Container } from "../../../UI";
import { ChatStackParamList } from "../../../navigation/types";

interface IProps {
  navigation?: StackScreenProps<ChatStackParamList>;
}

const Component: React.FC<IProps> = (props) => {
  const { useState } = React;
  const {} = props;
  const t = useTranslate();
  const { theme } = useTheme();
  const navigation = useNavigation();

  const addNewChannel = () => {
    navigation.navigate("ChannelStack", { screen: "ChannelEdit" });
  };

  return (
    <Container noPadding style={styles.container}>
      <Divider />
      <Button
        title={t("Add new Channel")}
        type="clear"
        icon={{
          name: "people-outline",
          type: "ionicon",
          size: theme.fontSizes?.medium,
        }}
        titleStyle={{
          fontSize: theme.fontSizes?.small,
        }}
        iconContainerStyle={{ padding: theme.Spacing?.medium }}
        onPress={addNewChannel}
      />
    </Container>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 150,
    width: "100%",
    alignItems: "flex-start",
    justifyContent: "flex-start",
  },
});

export default Component;
