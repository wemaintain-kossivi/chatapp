import * as React from "react";
import { StyleSheet, SafeAreaView } from "react-native";
import { Text, FAB, useTheme } from "react-native-elements";
import { User } from "sendbird";
import { useDispatch, useSelector } from "react-redux";

import { Container, CrispyList, ICrispyListItem } from "../../../UI";
import PanelFooter from "./PanelFooter";
import { userIsFetching, usersList } from "../../../store/selectors";
import { usersFetch } from "../../../store/actions";

interface IProps {}

const Component: React.FC<IProps> = (props) => {
  const { useEffect } = React;
  const {} = props;
  const { theme } = useTheme();
  const dispatch = useDispatch();

  const isLoading = useSelector(userIsFetching);
  const data = useSelector(usersList);

  useEffect(() => {
    refreshList();
  }, []);

  const refreshList = () => {
    dispatch(usersFetch());
  };

  return (
    <Container noPadding flex style={styles.container}>
      <SafeAreaView style={{ flex: 1, width: "100%" }}>
        <Container
          flex
          noPadding
          style={{ backgroundColor: theme.colors?.white }}
        >
          <CrispyList
            data={data}
            onRefresh={refreshList}
            onEndReached={refreshList}
            refreshing={isLoading}
            itemSubTitleStyle={{
              color: theme.colors?.grey3,
              fontSize: theme.fontSizes?.xSmall,
            }}
            emptyIcon="people-outline"
          />
        </Container>
        <PanelFooter />
      </SafeAreaView>
    </Container>
  );
};

const styles = StyleSheet.create({
  container: {
    width: "100%",
  },
});

export default Component;
