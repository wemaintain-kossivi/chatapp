import * as React from "react";
import { StyleSheet, useWindowDimensions } from "react-native";
import { Text, FAB, Header, useTheme } from "react-native-elements";
import { useNavigation } from "@react-navigation/native";
import { StackScreenProps } from "@react-navigation/stack";
import { useTranslate } from "react-polyglot";

import {
  Container,
  Splitter,
  HeaderTitle,
  HeaderActions,
  IHeaderIcon,
} from "../../UI";
import Panel from "../Chat/Panel";
import Channels from "../Channel/ChannelsList";

import { ChatStackParamList } from "../../navigation/types";

interface IProps {
  navigation?: StackScreenProps<ChatStackParamList>;
}

const Component: React.FC<IProps> = (props) => {
  const { useState } = React;
  const {} = props;
  const [panelIsVisible, setPanelIsVisible] = useState(false);
  const { theme } = useTheme();
  const navigation = useNavigation();
  const { width: screenWidth } = useWindowDimensions();
  const t = useTranslate();

  const addNewChannel = () => {
    navigation.navigate("ChannelStack", { screen: "ChannelEdit" });
  };

  const headerIcons: IHeaderIcon[] = [
    {
      name: "add-outline",
      color: theme.colors?.white,
      onPress: addNewChannel,
    },
    {
      name: "people-outline",
      color: theme.colors?.white,
      onPress: () => setPanelIsVisible(!panelIsVisible),
    },
  ];

  return (
    <Container noPadding flex>
      <Header
        centerComponent={
          <HeaderTitle
            title={t("Menu Channels")}
            subTitle=""
            color={theme.colors?.white}
          />
        }
        rightComponent={<HeaderActions icons={headerIcons} />}
        placement="left"
        backgroundColor={theme.colors?.primary}
      />
      <Splitter
        panelComponent={<Panel />}
        mainComponent={
          <Container flex noPadding style={{ width: screenWidth }}>
            <Channels />
          </Container>
        }
        panelIsVisible={panelIsVisible}
        onPressBackDrop={() => setPanelIsVisible(false)}
      />
    </Container>
  );
};

export default Component;
