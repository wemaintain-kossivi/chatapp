import * as React from "react";
import { StyleSheet } from "react-native";
import { useTranslate } from "react-polyglot";
import { useNavigation } from "@react-navigation/native";
import { StackScreenProps } from "@react-navigation/stack";
import { Header, useTheme } from "react-native-elements";
import { useSelector, useDispatch } from "react-redux";

import {
  Container,
  HeaderTitle,
  IHeaderIcon,
  HeaderActions,
  CrispyAvatar,
} from "../../UI";
import Messages from "../Message/MessagesList";
import MsgInput from "../Message/MsgInput";
import { ChatStackParamList } from "../../navigation/types";

import { channelTypeLabel } from "../../helpers";

import { selectedChannel } from "../../store/selectors";
import { joinChannel } from "../../store/actions";

interface IProps {
  navigation?: StackScreenProps<ChatStackParamList>;
}

const Component: React.FC<IProps> = (props) => {
  const { useState, useEffect } = React;
  const {} = props;
  const t = useTranslate();
  const navigation = useNavigation();
  const { theme } = useTheme();
  const dispatch = useDispatch();

  const channelActive = useSelector(selectedChannel);
  const { channel } = channelActive || {};

  useEffect(() => {
    enterChannel(); //required to fetch message after sb.disconnect
  }, []);

  const enterChannel = () => {
    if (channel) {
      dispatch(joinChannel(channel));
    }
  };

  const editChannel = () => {
    navigation.navigate("ChannelStack", { screen: "ChannelEdit" });
  };
  const addUsersToChannel = () => {
    navigation.navigate("ChannelStack", { screen: "ChannelEditUsers" });
  };
  const back = () => {
    navigation.goBack(null);
  };

  const headerIcons: IHeaderIcon[] = [
    {
      name: "people-circle-outline",
      onPress: addUsersToChannel,
    },
    {
      name: "close-outline",
      onPress: back,
    },
  ];

  return (
    <Container
      noPadding
      flex
      style={{ ...styles.container, backgroundColor: theme.colors?.white }}
    >
      <Header
        leftComponent={
          <CrispyAvatar
            avatarUrl={channel?.coverUrl}
            size="medium"
            onPress={back}
          />
        }
        centerComponent={
          <HeaderTitle
            title={channel?.name || ""}
            subTitle={channelTypeLabel(channel?.isOpenChannel())}
          />
        }
        rightComponent={<HeaderActions icons={headerIcons} />}
        placement="left"
      />

      <Container flex noPadding>
        <Messages />
      </Container>

      <MsgInput />
    </Container>
  );
};

const styles = StyleSheet.create({
  container: {
    width: "100%",
  },
});

export default Component;
