import * as React from "react";
import { StyleSheet } from "react-native";
import { StackScreenProps } from "@react-navigation/stack";
import { useTheme, LinearProgress } from "react-native-elements";
import { OpenChannel } from "sendbird";
import { useTranslate } from "react-polyglot";
import { useNavigation } from "@react-navigation/native";
import { useSelector, useDispatch } from "react-redux";
import { debounce } from "lodash";

import { Container, CrispyList, ICrispyListItem } from "../../UI";
import { ChatStackParamList } from "../../navigation/types";
import { channelIsFetching, channelsList } from "../../store/selectors";
import { channelFetch, channelSelect } from "../../store/actions";

interface IProps {
  navigation?: StackScreenProps<ChatStackParamList>;
}

const Screen: React.FC<IProps> = (props) => {
  const { useState, useEffect } = React;
  const [searchText, setSearchText] = useState("");
  const t = useTranslate();
  const navigation = useNavigation();
  const { theme } = useTheme();
  const dispatch = useDispatch();

  const isLoading = useSelector(channelIsFetching);
  const data = useSelector(channelsList);

  useEffect(() => {
    refreshList();
  }, []);

  const onTypingSearch = (val: string) => {
    setSearchText(val);

    debounce(() => dispatch(channelFetch(val)), 200)();
  };

  const refreshList = () => {
    dispatch(channelFetch());
  };

  const onPressChannelItem = (item: ICrispyListItem<OpenChannel>) => {
    dispatch(channelSelect(item.raw as OpenChannel));
    navigation.navigate("ChannelStack", { screen: "Channel" });
  };

  return (
    <Container noPadding flex>
      <CrispyList
        data={data}
        onPressItem={onPressChannelItem}
        showChevron
        defaultAvatarIcon="people-outline"
        onRefresh={refreshList}
        onEndReached={refreshList}
        refreshing={isLoading}
        showSearchBar
        onSearchChangeText={onTypingSearch}
        searchValue={searchText}
      />
      <Container noPadding style={{ position: "absolute", top: 0 }}>
        {isLoading && <LinearProgress color="secondary" />}
      </Container>
    </Container>
  );
};

const styles = StyleSheet.create({
  container: {
    width: "100%",
  },
});

export default Screen;
