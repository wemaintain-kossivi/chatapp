import * as React from "react";
import { View, FlatList, StyleSheet, SafeAreaView, Alert } from "react-native";
import { useTranslate } from "react-polyglot";
import { useNavigation } from "@react-navigation/native";
import { StackScreenProps } from "@react-navigation/stack";
import { useTheme, Text, Header } from "react-native-elements";
import { useDispatch, useSelector } from "react-redux";

import {
  ListItem,
  Container,
  CrispyList,
  HeaderTitle,
  HeaderActions,
  IHeaderIcon,
  ICrispyListItem,
} from "../../UI";
import { ChatStackParamList } from "../../navigation/types";
import { userIsFetching, usersList } from "../../store/selectors";
import { usersFetch } from "../../store/actions";

interface IProps {
  navigation?: StackScreenProps<ChatStackParamList>;
}

const Screen: React.FC<IProps> = (props) => {
  const { useState, useEffect } = React;
  const t = useTranslate();
  const navigation = useNavigation();
  const { theme } = useTheme();
  const dispatch = useDispatch();

  const isLoading = useSelector(userIsFetching);
  const data = useSelector(usersList);

  useEffect(() => {
    refreshList();
  }, []);

  const refreshList = () => {
    dispatch(usersFetch());
  };

  const cancelEdition = () => {
    navigation.goBack();
  };
  const headerIcons: IHeaderIcon[] = [
    {
      name: "close-outline",
      onPress: cancelEdition,
    },
  ];
  return (
    <Container flex style={{ ...styles.mask }} noPadding>
      <Container flex style={styles.container} noPadding>
        <Header
          centerComponent={
            <HeaderTitle title="Members" subTitle="--Work in progress" />
          }
          rightComponent={<HeaderActions icons={headerIcons} />}
          placement="left"
          containerStyle={{ paddingTop: 0, margin: 0 }}
        />

        <Container flex noPadding>
          <CrispyList
            data={data}
            showSearchBar
            inSelectMode
            validationIcon="checkmark-outline"
            onRefresh={refreshList}
            refreshing={isLoading}
            emptyIcon="person-outline"
          />
        </Container>
      </Container>
    </Container>
  );
};

const styles = StyleSheet.create({
  container: {
    width: "100%",
  },
  mask: {
    paddingTop: 100,
    backgroundColor: "#666666",
  },
});

export default Screen;
