import * as React from "react";
import { StyleSheet, Alert, TextStyle } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { StackScreenProps } from "@react-navigation/stack";
import { useTranslate } from "react-polyglot";
import {
  Text,
  FAB,
  Header,
  Input,
  useTheme,
  Divider,
  Icon,
  Button,
} from "react-native-elements";
import { useDispatch } from "react-redux";

import { ChatStackParamList } from "../../navigation/types";
import {
  Container,
  HeaderTitle,
  HeaderActions,
  IHeaderIcon,
  KeyboardSafeArea,
  CrispyAvatar,
  // CrispyList,
  // ICrispyListItem,
} from "../../UI";
import { channelEdit } from "../../store/actions";

const KEYBOARDSAFE_OFFSET = 100;
interface IProps {
  navigation?: StackScreenProps<ChatStackParamList>;
}

const Component: React.FC<IProps> = (props) => {
  const {} = props;
  const { useState } = React;
  const [channelName, setChannelName] = useState("");
  const navigation = useNavigation();
  const t = useTranslate();
  const { theme } = useTheme();
  const dispatch = useDispatch();

  const cancelEdition = () => {
    navigation.goBack();
  };

  const validEdition = () => {
    dispatch(
      channelEdit({
        chanelName: channelName,
      })
    );
    navigation.goBack();
  };
  const editUsers = () => {
    navigation.navigate("ChannelEditUsers");
  };

  const headerIcons: IHeaderIcon[] = [
    {
      name: "close-outline",
      onPress: cancelEdition,
    },
  ];

  const sectionTextStyle: TextStyle = {
    fontSize: theme.fontSizes?.small,
    marginVertical: theme.Spacing?.medium,
    alignSelf: "flex-start",
  };

  return (
    <Container noPadding flex style={{ backgroundColor: theme.colors?.white }}>
      <Header
        centerComponent={<HeaderTitle title={t("Edit")} subTitle="" />}
        rightComponent={<HeaderActions icons={headerIcons} />}
        placement="left"
      />

      <Container flex>
        <Container>
          <CrispyAvatar size="large" icon="people" />
        </Container>

        <KeyboardSafeArea offset={KEYBOARDSAFE_OFFSET}>
          <Container>
            <Divider />
            <Button
              title={t("Channel Form channelTitle")}
              type="clear"
              titleStyle={sectionTextStyle}
              icon={{
                name: "pencil-outline",
                type: "ionicon",
                size: theme.fontSizes?.medium,
              }}
              iconContainerStyle={{
                padding: theme.Spacing?.small,
              }}
              containerStyle={{ alignSelf: "flex-start" }}
            />

            <Input
              inputContainerStyle={{
                borderRadius: 5,
              }}
              value={channelName}
              onChangeText={setChannelName}
            />
          </Container>
          <FAB
            placement="right"
            icon={{
              type: "ionicon",
              name: "checkmark-outline",
              color: `${theme.colors?.white}`,
            }}
            onPress={validEdition}
          />
        </KeyboardSafeArea>

        {/**
   * WIP
   * 
   *  <Container flex>
          <Divider />
          <Button
            title={t("Add users to channel")}
            type="clear"
            icon={{
              name: "person-add-outline",
              type: "ionicon",
              size: theme.fontSizes?.medium,
            }}
            titleStyle={sectionTextStyle}
            iconContainerStyle={{ padding: theme.Spacing?.small }}
            containerStyle={{ alignSelf: "flex-start" }}
            onPress={editUsers}
          />
          <CrispyList data={usersList} />
        </Container>
   */}
      </Container>
    </Container>
  );
};

const styles = StyleSheet.create({
  container: {},
});

export default Component;
