import { StackScreenProps } from "@react-navigation/stack";
import * as React from "react";
import { StyleSheet, TouchableOpacity, View } from "react-native";
import { Text } from "react-native-elements";
import { useTranslate } from "react-polyglot";

import { RootStackParamList } from "../navigation/types";
import { EmptyComponent, Container } from "../UI";

interface IProps {}
const OfflineScreen = () => {
  const t = useTranslate();
  return (
    <Container flex>
      <EmptyComponent
        icon="cloud-offline-outline"
        title={t("Internet error")}
        text={t("Seems to be offline")}
      />
    </Container>
  );
};

export default OfflineScreen;
